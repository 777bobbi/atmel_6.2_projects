#include <mega8.h>
#include <alcd.h>
#include <delay.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define RADIUS 15 //mm
#define CORRECTION 1.2 //mm // devise version1 - 0.02mm, verion2 - 1.05mm
#define CIRCLE_LENGTH (2 * 3.14 * RADIUS + CORRECTION)
#define IMPULSES_PER_REVOLUTION 60 
#define METERS_PER_PULS ( CIRCLE_LENGTH  / IMPULSES_PER_REVOLUTION  / 1000 )
#define INT_CONSTRAINT 32767
#define METERS_PER_COEFFICIENT (METERS_PER_PULS * INT_CONSTRAINT)

int impulses = 0;
int coefficient = 0;
int direction = 1; // 1 - right, -1 - left

void impulsesIncrement() {
 if ( impulses == INT_CONSTRAINT ) {
  impulses = 0;
  coefficient += 1;
 }
 impulses += 1;
 direction = 1;
}

void impulsesDecrement() {
 if ( impulses == -INT_CONSTRAINT ) {
  impulses = 0;
  coefficient -= 1;
 }
 impulses -= 1;
 direction = -1;
}

// External Interrupt 0 service routine
interrupt [EXT_INT0] void ext_int0_isr(void)
{
  if ( PIND.1 == 1 ) {
    impulsesIncrement();
  } else {
    impulsesDecrement();
  }
}

// External Interrupt 1 service routine
interrupt [EXT_INT1] void ext_int1_isr(void)
{
  if ( PIND.1 == 1 ) {
    impulsesDecrement();
  } else {
    impulsesIncrement();
  }
}

void setLengthStr(char *destination, float length) {
  char centimeters[3];
  char millimeters[2];
  char * pch;
  int position;
  
  centimeters[2] = 0;
  millimeters[1] = 0;
  
  sprintf(destination, "%.3f", length);
  
  pch = strstr(destination, ".");
  position = pch - destination + 1;

  strncpy(centimeters, destination + position, 2);
  strncpy(millimeters, destination + position + 2, 1);
  
  sprintf(destination, "%.0fm %scm %smm\n", floor(length), centimeters, millimeters);
}

float absf(float n) {
  if ( n < 0 ) {
    return n * -1;
  }
  return n;
}


void main(void)
{
char lengthStr[16];
float length;
char directionLeft[4] = "<<<";
char directionRight[4] = ">>>";
char buff[16];

//direction
DDRB=0x00;
DDRC=0x00;
DDRD=0x00;

PORTB=0x00;
PORTC=0x00;
PORTD=0x00; 

// External Interrupt(s) initialization
// INT0: On
// INT0 Mode: Rising Edge
// INT1: On
// INT1 Mode: Falling Edge
GICR|=0xC0;
MCUCR=0x0B;
GIFR=0xC0;

// Timer(s)/Counter(s) Interrupt(s) initialization
TIMSK=0x00;

// Global enable interrupts
#asm("sei")

// Alphanumeric LCD initialization
// Connections specified in the
// Project|Configure|C Compiler|Libraries|Alphanumeric LCD menu:
// RS - PORTC Bit 0
// RD - PORTC Bit 6
// EN - PORTC Bit 1
// D4 - PORTC Bit 2
// D5 - PORTC Bit 3
// D6 - PORTC Bit 4
// D7 - PORTC Bit 5
// Characters/line: 16
lcd_init(16);

while (1)
  {
    lcd_clear();
    length = absf(METERS_PER_PULS * impulses + METERS_PER_COEFFICIENT * coefficient);
    setLengthStr(lengthStr, length);    
    lcd_puts(lengthStr);
    
    lcd_gotoxy(0, 1);
    if ( direction == 1 ) {
      lcd_puts(directionRight);  
    } else {
      lcd_puts(directionLeft);
    }
    
    lcd_gotoxy(5, 1);
    sprintf(buff, "%d", impulses);
    lcd_puts(buff);
    
    delay_ms(50);
  }
}
