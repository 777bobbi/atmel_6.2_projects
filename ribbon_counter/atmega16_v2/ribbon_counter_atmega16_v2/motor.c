#include "motor.h"

int current_speed_in_percenatge = 0;
int smooth_speed_increase_enabled = 0;
uint32_t speed_limit_in_percentage = 100; // motor_max_speed
uint32_t speed_limit_in_percentage_smooth_increase = 0;
unsigned long earlier_millis_smooth_start = 0;
int time_interval_for_speed_increase_ms = TIME_INTERVAL_FOR_SPEED_INCREASE_MS; // speed increase every 1000 ms

void motor_init() {
	// OC2 PWM to motor (driver)
	OUT(MOTOR_PWM_PORT, MOTOR_PWM_PIN);
	
	/*
		Initialization of PWM:
		start timer:
		WGM20			- set 'PWM, Phase Correct' mode
		COM21, COM20	- set OC2 on compare match when up-counting. Clear OC2 on compare match when down-counting.
		CS20			- clkI/O (No prescaling)
		CS21, CS20		- clkI/64 = 125 KHz (From prescaler)
		CS22			- clkI/256 = 31250 Hz (From prescaler)
		CS22, CS20		- clkI/1024 = 7812 Hz (From prescaler)
	*/
	TCCR2 = 1<<WGM20|1<<COM21|1<<COM20|1<<CS20;
	OCR2 = MOTOR_STOP_PWM; // initial value (motor stop) on PWM pin output
	
	speed_limit_in_percentage = storage_motor_get_speed_limit();
}

void motor_set_speed_in_percentage(unsigned long percentage) {
	if ( percentage > speed_limit_in_percentage ) {
		percentage = speed_limit_in_percentage;
	}
	
	if (percentage <= 0) {
		current_speed_in_percenatge = 0;
		smooth_speed_increase_enabled = 0;
		time_interval_for_speed_increase_ms = TIME_INTERVAL_FOR_SPEED_INCREASE_MS;
		OCR2 = MOTOR_STOP_PWM;
		return;
	}
	
	current_speed_in_percenatge = percentage;
 	OCR2 = CONVERT_MOTOR_SPEED_PERCENTAGE_TO_PWM(percentage); // Set PWM
}

int motor_is_working() {
	return current_speed_in_percenatge > 0;
}

void motor_handler_smooth_speed_increase(unsigned long current_millis) {
	if (current_millis - earlier_millis_smooth_start >= time_interval_for_speed_increase_ms) {
		int speed = current_speed_in_percenatge;
		earlier_millis_smooth_start = current_millis;
		
		if ( !smooth_speed_increase_enabled ) {
			return;
		}
		
		if ( ++speed <= speed_limit_in_percentage_smooth_increase && speed <= speed_limit_in_percentage ) {
			motor_set_speed_in_percentage(speed);
		} else {
			smooth_speed_increase_enabled = 0;
			time_interval_for_speed_increase_ms = TIME_INTERVAL_FOR_SPEED_INCREASE_MS;
		}
	}
}

void motor_set_speed_in_percentage_smooth_increase(unsigned long percentage, int speed_increase_every_ms) {
	time_interval_for_speed_increase_ms = speed_increase_every_ms;
	speed_limit_in_percentage_smooth_increase = percentage;
	smooth_speed_increase_enabled = 1;
}

void motor_set_speed_limit_in_percentage(uint32_t limit) {
	if (limit > 100) {
		limit = 100;
	}
	
	speed_limit_in_percentage = limit;
	storage_motor_set_speed_limit(limit);
}

int motor_get_speed_limit() {
	return speed_limit_in_percentage;
}