/*
 * storage.h
 *
 * set/get variables to/from EEPROM
 *
 * Created: 3/24/2015 10:17:13 PM
 *  Author: Vladimir
 */

 #ifndef STORAGE_H
 #define STORAGE_H

 #include "app.h" // maybe must not be included in storage (storage must be as separate model without relations)
 #include <avr/eeprom.h>
 
 void storage_motor_set_speed_limit(uint32_t);
 uint32_t storage_motor_get_speed_limit();
 
 void storage_set_diametr_correction_mm(float);
 float storage_get_diametr_correction_mm();
 
 void storage_set_distance_not_rewinded_ribbon_mm(float);
 float storage_get_distance_not_rewinded_ribbon_mm();
 
 void storage_set_distance_not_rewinded_ribbon_enable(uint32_t);
 uint32_t storage_get_distance_not_rewinded_ribbon_enable();

 #endif // STORAGE_H
