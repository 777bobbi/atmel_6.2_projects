/*
 * helper.c
 *
 *	Helper function
 *
 * Created: 3/1/2015 8:38:01 PM
 *  Author: Vladimir
 */ 

#include "app.h"
#include "helper.h"

void dump_int(const int n, int x, int y, int lcd_clear) {
	char buf[16];
	
	memset(buf, 0, sizeof(buf)); // clear buffer and nulled last byte
	
	sprintf(buf, "%d ", n);
	
	if ( lcd_clear ) {
		lcd_clrscr();
	}
	
	lcd_gotoxy(x, y);
	lcd_puts(buf);
	//uart_puts(buf);
}

void dump_double(const double n, int x, int y) {
	char str[16];
	str[0] = '\0';

	//lcd_clrscr();
	lcd_gotoxy(x, y);
	sprintf(str, ".3f: %.3f", n);
	lcd_puts(str);
}

void dump_str(char *str, int x, int y) {
	lcd_clrscr();
	lcd_gotoxy(x, y);
	lcd_puts(str);
	
	//uart_puts(str);
}

char *byte_to_binary(int x)
{
	static char b[9];
	b[0] = '\0';

	int z;
	for (z = 128; z > 0; z >>= 1)
	{
		strcat(b, ((x & z) == z) ? "1" : "0");
	}

	return b;
}
