/*
 * helper.h
 *
 * Created: 3/23/2015 12:55:42 AM
 *  Author: Vladimir
 */ 

#ifndef HELPER_H
#define HELPER_H

void dump_int(const int, int x, int y, int clear_display);
void dump_double(const double n, int x, int y);
void dump_str(char *str, int x, int y);
char *byte_to_binary(int x);

#endif /* HELPER_H */