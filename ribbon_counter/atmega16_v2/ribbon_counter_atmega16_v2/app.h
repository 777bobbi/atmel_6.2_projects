/*
 * app.h
 *
 * Created: 2/28/2015 11:09:03 PM
 *  Author: Vladimir
 */

#ifndef APP_H_
#define APP_H_

#define F_CPU 8000000UL
#define UART_BAUD_RATE 9600

#include <stdint.h> // int64_t // inttypes.h
#include <stdbool.h> // bool
#include <stdio.h>	// sprintf
#include <stdlib.h> //itoa
#include <string.h> // strncpy, strstr
#include <math.h> // floor, round
#include <avr/io.h>
//#include <avr/iom16.h>
#include <avr/interrupt.h> // ISR
#include <util/delay.h> // for debug
#include "lib/uart/uart.h" // for debug
#include "lib/l74hc165/l74hc165.h" // don't forget configure (74HC165 shift registers)
#include "lib/lcd/lcd.h" // don't forget configure
#include "bitwise.h" // Bitwise Operation Simplification Defines
#include "helper.h" // debug functions
#include "storage.h" // there is a variables storage in EEPROM

// Metering unit defines
#define RADIUS_MM 15
#define CIRCLE_LENGTH(CORRECTION) (2 * 3.14 * RADIUS_MM + CORRECTION)
#define IMPULSES_PER_REVOLUTION 60
#define METERS_PER_PULS(CIRCLE_LENGTH) ( CIRCLE_LENGTH  / IMPULSES_PER_REVOLUTION  / 1000 )
#define TYPICAL_DISTANCE_NOT_REWINDED_RIBBON_MM 150
#define TONGUE_LENGTH_1_M 1
#define TONGUE_LENGTH_2_M 2

// Buttons defines
#define CONTACT_BOUNCE_DELEY 25 //ms
#define MIN_TRESHOLD_FIRST_ACTION 2 // times of the polls pressed button. One poll == CONTACT_BOUNCE_DELEY
#define SPEED_OF_LINER_INCREASE_VALUE 10 // + - speed slower, - - speed faster
#define BUTTONS_SEQUENCE_STARTS_FROM_ONE 1
#define BUTTONS_QUANTITY 14
#define BUTTON_RIBBON_LENGTH_1M 1
#define BUTTON_RIBBON_LENGTH_5M 2
#define BUTTON_RIBBON_LENGTH_20M 3
#define BUTTON_RIBBON_LENGTH_1CM 4
#define BUTTON_RIBBON_LENGTH_10CM 5
#define BUTTON_RIBBON_LENGTH_50CM 6
#define BUTTON_REWIND_PAUSE 7
#define BUTTON_REWIND_WHILE_PRESSED 8
#define BUTTON_DIAMETR_DECREMENT 9
#define BUTTON_DIAMETR_INCREMENT 10
#define BUTTON_DISTANCE_DECREMENT 11
#define BUTTON_DISTANCE_INCREMENT 12
#define BUTTON_MAX_RPM_DECREMENT 13
#define BUTTON_MAX_RPM_INCREMENT 14
#define led_green_on()  SET(A,1)
#define led_green_off()  CLR(A,1)
#define led_yellow_on()  SET(A,2)
#define led_yellow_off()  CLR(A,2)
struct button {
	int index;
	int times;
};

// Function prototypes system
unsigned long millis();

// Function prototypes common
void io_init(void);
void timer1_init(void);
void external_interrupts_init(void);

// Function prototypes metering unit
void impulsesIncrement(void);
void impulsesDecrement(void);
void setLengthStr(char *destination, double length, bool show_mm);
double absf(double n);
void show_count_results(void);
void lcd_diametr_correction(double diametr_correction_mm);
void lcd_distance_not_rewinded_ribbon(double distance_not_rewinded_ribbon_mm);
void handle_auto_rewind(void);
int is_auto_rewind_enable();
double get_ribbon_already_rewinded_length(void);

// Function prototypes keyboard buttons
void buttons_init();
void handle_buttons();
int poll_buttons();
void button_ribbon_length_1m();
void button_ribbon_length_5m();
void button_ribbon_length_20m();
void button_ribbon_length_1cm();
void button_ribbon_length_10cm();
void button_ribbon_length_50cm();
void button_rewind_pause();
void button_rewind_while_pressed();
void button_diametr_decrement();
void button_diametr_increment();
void button_distance_decrement();
void button_distance_increment();
void button_max_rpm_decrement();
void button_max_rpm_increment();
void button_unpressed_handler(int button_pressed_index);
void lcd_max_rpm(uint32_t speed);

#endif /* APP_H_ */