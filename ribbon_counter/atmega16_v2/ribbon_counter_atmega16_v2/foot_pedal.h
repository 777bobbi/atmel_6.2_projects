#ifndef FOOT_PEDAL_H
#define FOOT_PEDAL_H

#include "app.h"
#include "motor.h"

#define FOOT_PEDAL_PORT A
#define FOOT_PEDAL_PIN PA0
#define ONE_SECOND 1000 // ms

void foot_pedal_init();
void foot_pedal_listener();
void onkeypress();
void onclick();

#endif // FOOT_PEDAL_H