/*
 * init.c
 *
 * Created: 3/1/2015 6:38:27 PM
 *  Author: Vladimir
 */ 

#include "app.h"

volatile unsigned long timer1_millis = 0;    // millisecond count

unsigned long millis()
{
	unsigned long m;
	char oldSREG = SREG;
	
	cli(); // disable interrupts while we read timer0_millis or we might get an inconsistent value (e.g. in the middle of a write to timer0_millis)
	m = timer1_millis;
	SREG = oldSREG;

	return m;
}

void io_init() {
	// LCD
	//Defines stored in lcd.h
	
	// Buttons
	// Keyboard defines stored in l74hc165.h
	
	// Hall sensors
	IN(D,2); // INT0
	IN(D,3); // INT1 
	IN(D,4); // plain input
	
	// LEDs
	OUT(A,1); // LED green
	OUT(A,2); // LED yellow
}

/*
	used for hall sensor
*/
void external_interrupts_init(void) {
	//INT0_vect
	MCUCR |= 1<<ISC01|1<<ISC00;	// Trigger INT0 on rising edge.
	GICR |= 1<<INT0;			// Enable interrupt INT0
	GIFR |= 1<<INT0;			// Cleared flag by writing a logical one to it. This scaffolding need to avoid first interrupt occur, because interrupt pin set pull up.
	
	//INT1_vect
	MCUCR |= 1<<ISC11;	// Trigger INT1 on falling edge.
	GICR |= 1<<INT1;	// Enable interrupt INT1
	GIFR |= 1<<INT1;	// Cleared flag by writing a logical one to it. This scaffolding need to avoid first interrupt occur, because interrupt pin set pull up.
}

/*
	Used for count millis
	Trigger interrupt every one ms
*/
void timer1_init(void) {
	TCCR1B	|= 1<<WGM12;			//Timer in CTC mode
	TCCR1B	|= 1<<CS11|1<<CS10;		//Prescaler to 1:64/F_CPU:64
	OCR1A	= 124;					//Value to have an compare at every 1ms = 1000Hz (Target Timer Count = (((Input Frequency / Prescaler) / Target Frequency) - 1) -> Target Timer Count = (((8000000Hz / 64) / 1000Hz) - 1) = 124)
	TIMSK	= 1<<OCIE1A;			//Enable timer interrupts
}

/*
	External Interrupt service routine
	Configured as rising edge
*/
ISR(INT0_vect) {
	if ( READ(D, 4) ) {
		impulsesIncrement();
	} else {
		impulsesDecrement();
	}
}

/*
	External Interrupt service routine
	Configured as falling edge
*/
ISR(INT1_vect) {
	if ( READ(D, 4) ) {
		impulsesDecrement();
	} else {
		impulsesIncrement();
	}
}

/*
	Internal interrupt service routine
	Increment millis quantity every one millisecond
*/
ISR(TIMER1_COMPA_vect) {
	timer1_millis += 1;
}
