/*
 * ribbon_counter_atmega16_v2.c
 * 
 
 to do improvements:
 - decouple code in main.c in a models: 'metering_unit', 'keyboard'
 
 * Created: 9/12/2013 7:23:22 PM
 *  Author: Vladimir
 */ 

#include "app.h"
#include "motor.h"
#include "foot_pedal.h"

// Global variables metering unit
long int impulses = 0;
int direction = 1; // 1 - right, -1 - left
double length_to_rewind = 0;

// Global variables buttons
uint8_t get_icarray[L74HC165_ICNUMBER];
volatile unsigned int button_pressed = 0; // current pressed button
volatile int button_pressed_polls = 0; // times poll pressed button
void (*buttons_service_routines[BUTTONS_QUANTITY+BUTTONS_SEQUENCE_STARTS_FROM_ONE])(void); // Array of Function Pointers (pt2Function)
int auto_rewind_enable = 0;
int rewind_while_pressed_enable = 0;
int settings_button_pressed = 0;
struct button button_unpressed = {0, 0};
void count_button_unpressed();

uint32_t max_motor_speed_in_percentage = 0;
float diametr_correction_mm = 0;
float distance_not_rewinded_ribbon_mm = 0;
uint32_t distance_not_rewinded_ribbon_enable = 0;

int main(void) {
	unsigned long earlier_millis = 0;
	
	max_motor_speed_in_percentage = storage_motor_get_speed_limit();
	diametr_correction_mm = storage_get_diametr_correction_mm();
	distance_not_rewinded_ribbon_mm = storage_get_distance_not_rewinded_ribbon_mm();
	distance_not_rewinded_ribbon_enable = storage_get_distance_not_rewinded_ribbon_enable();
	
	io_init();
	external_interrupts_init();
	timer1_init();
	buttons_init();
	l74hc165_init();
	motor_init();
	foot_pedal_init();
	//uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	sei();	// Global enable interrupts
	lcd_init(LCD_DISP_ON);
	
	for (;;) {
		unsigned long current_millis = millis();
		
		if (current_millis - earlier_millis >= CONTACT_BOUNCE_DELEY) {
			earlier_millis = current_millis;
			
			handle_buttons();
			foot_pedal_listener(); // handle_foot_pedal();
			show_count_results();
			handle_auto_rewind();
		}
		
		motor_handler_smooth_speed_increase(current_millis);
	}
	
	return 0;
}

void buttons_init() {
	buttons_service_routines[BUTTON_RIBBON_LENGTH_1M]		= button_ribbon_length_1m;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_5M]		= button_ribbon_length_5m;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_20M]		= button_ribbon_length_20m;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_1CM]		= button_ribbon_length_1cm;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_10CM]		= button_ribbon_length_10cm;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_50CM]		= button_ribbon_length_50cm;
	buttons_service_routines[BUTTON_REWIND_PAUSE]			= button_rewind_pause;
	buttons_service_routines[BUTTON_REWIND_WHILE_PRESSED]	= button_rewind_while_pressed;
	buttons_service_routines[BUTTON_DIAMETR_DECREMENT]		= button_diametr_decrement;
	buttons_service_routines[BUTTON_DIAMETR_INCREMENT]		= button_diametr_increment;
	buttons_service_routines[BUTTON_DISTANCE_DECREMENT]		= button_distance_decrement;
	buttons_service_routines[BUTTON_DISTANCE_INCREMENT]		= button_distance_increment;
	buttons_service_routines[BUTTON_MAX_RPM_DECREMENT]		= button_max_rpm_decrement;
	buttons_service_routines[BUTTON_MAX_RPM_INCREMENT]		= button_max_rpm_increment;
}

void show_count_results() {
	char lengthStr[16];
	double length;
	char directionLeft[4] = "<<<";
	char directionRight[4] = ">>>";
	
	if ( settings_button_pressed ) {
		return;
	}
	
	// need to rewind
	lcd_clrscr();
	setLengthStr(lengthStr, length_to_rewind, false);
	lcd_puts(lengthStr);
	
	// counted
	length = get_ribbon_already_rewinded_length();
	setLengthStr(lengthStr, length, true);
	lcd_gotoxy(0, 1);
	lcd_puts(lengthStr);
	
	// direction
	lcd_gotoxy(13, 1);
	if ( direction == 1 ) {
		lcd_puts(directionRight);
	} else {
		lcd_puts(directionLeft);
	}
}

double get_ribbon_already_rewinded_length(void) {
	return absf(METERS_PER_PULS(CIRCLE_LENGTH(diametr_correction_mm)) * impulses) + (distance_not_rewinded_ribbon_enable? distance_not_rewinded_ribbon_mm/1000 : 0);
}

void impulsesIncrement() {
	impulses += 1;
	direction = 1;
}

void impulsesDecrement() {
	impulses -= 1;
	direction = -1;
}

double absf(double n) {
	if ( n < 0 ) {
		return n * -1;
	}
	return n;
}

/*
	length = 121.429;
	output = 121m 42cm 9mm
*/
void setLengthStr(char *destination, double length, bool show_mm) {
	char meters[3];
	char centimeters[3];
	char millimeters[2];
	char * pch;
	int point_position;
	
	memset(destination, 0, sizeof(destination)); //clear destination string, for output and nulled last byte. The printf output will be: ''. If set last element in array to: '\0' or 0, the printf output will by: 'qwe' - for instance
	memset(meters, 0, sizeof(meters));
	centimeters[sizeof(centimeters) - 1] = 0;
	millimeters[sizeof(millimeters) - 1] = 0;
	
	sprintf(destination, "%.3lf", length);

	pch = strstr(destination, ".");
	point_position = pch - destination + 1;

	strncpy(meters, destination, point_position - 1);
	strncpy(centimeters, destination + point_position, 2);
	strncpy(millimeters, destination + point_position + 2, 1);
	
	if ( show_mm ) {
		sprintf(destination, "%sm %scm %smm\n", meters, centimeters, millimeters); 
	} else {
		sprintf(destination, "%sm %scm  %ld\n", meters, centimeters, impulses);
	}
}

/*
	every CONTACT_BOUNCE_DELEY ms poll buttons
	for every pressed button call according handler function
*/
void handle_buttons() {
	int current_button_pressed = poll_buttons();
	
	if ( current_button_pressed == button_pressed && button_pressed != 0 ) { //contact bounce handle
		button_pressed_polls += 1;
		
		if ( button_pressed_polls == MIN_TRESHOLD_FIRST_ACTION ) { // execute button service routine first press
			buttons_service_routines[current_button_pressed]();
			count_button_unpressed();
		}
		if ( button_pressed_polls % SPEED_OF_LINER_INCREASE_VALUE == 0 ) { // adjust speed of linear increase value when button hold
			buttons_service_routines[current_button_pressed]();
		}
	} else if ( current_button_pressed ) { //first button poll
		button_pressed = current_button_pressed;
		button_pressed_polls = 1;
	} else { //buttons unpressed
		button_unpressed_handler(button_pressed);
		button_pressed = 0;
		button_pressed_polls = 0;
	}
}

/*
	poll buttons 
	return: current pressed button id starts from less bit or 0 if no one button was pressed
*/
int poll_buttons() {
	uint8_t bytearray[L74HC165_ICNUMBER];
	const uint8_t pressed = 0; // 0 - button pressed, 1 - button unpressed
	int button_pressed_number = 1;

	l74hc165_read(bytearray); //read data
	
	for(int i = 0; i < L74HC165_ICNUMBER; i++) {
		for (int button = 1; button <= 128; button <<= 1, button_pressed_number++) {
			if ( (button & bytearray[i]) == pressed && button_pressed_number <= BUTTONS_QUANTITY ) {
				return button_pressed_number;
			}
		}
	}
	
	return 0;
}

void count_button_unpressed() {
	if ( button_unpressed.times == 0 ) {
		button_unpressed.index = button_pressed;
		button_unpressed.times += 1;
	} else if ( button_unpressed.index == button_pressed ) {
		button_unpressed.times += 1;
	} else { // unpressed another button
		button_unpressed.index = button_pressed;
		button_unpressed.times = 1;
	}
}

void handle_auto_rewind() {
	if ( !auto_rewind_enable || rewind_while_pressed_enable || length_to_rewind <= 0 ) {
		return;
	}
	
	double already_rewinded = get_ribbon_already_rewinded_length();
	
	if ( already_rewinded >= length_to_rewind) {
		auto_rewind_enable = 0;
		motor_set_speed_in_percentage(0);
		return;
	}
	if ( length_to_rewind - already_rewinded < TONGUE_LENGTH_1_M ) { // if length to rewind < 1 m reduce the speed to 10%
		motor_set_speed_in_percentage(10);
	}
	if ( length_to_rewind - already_rewinded < TONGUE_LENGTH_2_M ) { // if length to rewind < 2 m reduce the speed to 15%
		motor_set_speed_in_percentage(15);
	}
}

void button_ribbon_length_1m() {
	length_to_rewind += 1;
}
void button_ribbon_length_5m() {
	length_to_rewind += 5;
}
void button_ribbon_length_20m() {
	length_to_rewind += 20;
}
void button_ribbon_length_1cm() {
	length_to_rewind += 0.01;
}
void button_ribbon_length_10cm() {
	length_to_rewind += 0.1;
}
void button_ribbon_length_50cm() {
	length_to_rewind += 0.5;
}

int is_auto_rewind_enable() {
	return auto_rewind_enable;
}

void button_rewind_pause() {
	if ( auto_rewind_enable ) { // pause
		auto_rewind_enable = 0;
		motor_set_speed_in_percentage(0);
		led_green_off();
		led_yellow_on();
	} else { // start auto rewind
		auto_rewind_enable = 1;
		motor_set_speed_in_percentage_smooth_increase(70, 100);
		led_green_on();
		led_yellow_off();
	}
}
void button_rewind_while_pressed() {
	rewind_while_pressed_enable = 1;
	led_green_on();
	motor_set_speed_in_percentage(70);
}

void button_diametr_decrement() {
	diametr_correction_mm -= 0.01;
	
	lcd_diametr_correction(diametr_correction_mm);
}
void button_diametr_increment() {
	diametr_correction_mm += 0.01;
	
	lcd_diametr_correction(diametr_correction_mm);
}

void button_distance_decrement() {
	distance_not_rewinded_ribbon_mm -= 1;
	
	lcd_distance_not_rewinded_ribbon(distance_not_rewinded_ribbon_mm);
}
void button_distance_increment() {
	distance_not_rewinded_ribbon_mm += 1;
	
	lcd_distance_not_rewinded_ribbon(distance_not_rewinded_ribbon_mm);	
}

void button_max_rpm_decrement() {
	max_motor_speed_in_percentage += 5;
	
	if (max_motor_speed_in_percentage > 100) {
		max_motor_speed_in_percentage = 100;
	}
	
	lcd_max_rpm(max_motor_speed_in_percentage);
}
void button_max_rpm_increment() {
	max_motor_speed_in_percentage -= 5;
	
	if (max_motor_speed_in_percentage < 50) {
		max_motor_speed_in_percentage = 50;
	}
	
	lcd_max_rpm(max_motor_speed_in_percentage);
}

void button_unpressed_handler(int button) {	
	if ( button == BUTTON_REWIND_WHILE_PRESSED ) {
		rewind_while_pressed_enable = 0;
		led_green_off();
		motor_set_speed_in_percentage(0);
	}
	if ( button == BUTTON_MAX_RPM_DECREMENT || button == BUTTON_MAX_RPM_INCREMENT ) {
		settings_button_pressed = 0;
		motor_set_speed_limit_in_percentage(max_motor_speed_in_percentage);
		_delay_ms(500);
		button_unpressed.times = 0;
	}
	if ( button == BUTTON_DIAMETR_DECREMENT || button == BUTTON_DIAMETR_INCREMENT ) {
		settings_button_pressed = 0;
		storage_set_diametr_correction_mm(diametr_correction_mm);
		_delay_ms(500);
	}
	if ( button == BUTTON_DISTANCE_DECREMENT || button == BUTTON_DISTANCE_INCREMENT ) {
		settings_button_pressed = 0;
		storage_set_distance_not_rewinded_ribbon_mm(distance_not_rewinded_ribbon_mm);
		
		if ( button_unpressed.times == 5 ) {
			distance_not_rewinded_ribbon_enable = button_unpressed.index == BUTTON_DISTANCE_INCREMENT;
			distance_not_rewinded_ribbon_mm = distance_not_rewinded_ribbon_enable ? distance_not_rewinded_ribbon_mm - 5.0 : distance_not_rewinded_ribbon_mm + 5.0;
			
			storage_set_distance_not_rewinded_ribbon_mm(distance_not_rewinded_ribbon_mm);
			storage_set_distance_not_rewinded_ribbon_enable(distance_not_rewinded_ribbon_enable);
			
			lcd_clrscr();
			lcd_puts("Const distance:");
			lcd_gotoxy(0,1);
			lcd_puts(distance_not_rewinded_ribbon_enable ? "On" : "Off");
			_delay_ms(500);
		}
		
		_delay_ms(500);
	}
}

void lcd_max_rpm(uint32_t speed) {
	settings_button_pressed = 1;
	char buf[16];
	buf[0] = '\0';
	
	lcd_clrscr();
	sprintf(buf, "max speed: %d%%", (int)speed);
	lcd_puts(buf);
}

void lcd_diametr_correction(double diametr_correction_mm) {
	settings_button_pressed = 1;
	char buf[16];
	buf[0] = '\0';
	
	lcd_clrscr();
	sprintf(buf, "%.2fmm+%.2fmm", CIRCLE_LENGTH(0), diametr_correction_mm);
	lcd_puts(buf);
	lcd_gotoxy(0,1);
	sprintf(buf, "%.2fmm", CIRCLE_LENGTH(diametr_correction_mm));
	lcd_puts(buf);
}

void lcd_distance_not_rewinded_ribbon(double distance_not_rewinded_ribbon_mm) {
	settings_button_pressed = 1;
	char buf[16];
	buf[0] = '\0';
	
	lcd_clrscr();
	sprintf(buf, "status: %s", distance_not_rewinded_ribbon_enable ? "on" : "off"); 
	//sprintf(buf, "pols: %d", button_pressed_polls);
	lcd_puts(buf);
	lcd_gotoxy(0,1);
	sprintf(buf, "length + : %.0fmm", distance_not_rewinded_ribbon_mm);
	lcd_puts(buf);
}