#ifndef BITWISE_H
#define BITWISE_H

#define CONCAT(x, y) x ## y // If you wont write a variable definition in some header file like: #define LED_PORT D

#define CLR(port,pin)	CONCAT(PORT, port)	&= ~(1<<pin)
#define SET(port,pin)	CONCAT(PORT, port)	|=  (1<<pin)
#define TOGL(port,pin)	CONCAT(PORT, port)	^=  (1<<pin)
#define READ(port,pin)	CONCAT(PIN, port)	&   (1<<pin)
#define OUT(port,pin)	CONCAT(DDR, port)	|=  (1<<pin)
#define IN(port,pin)	CONCAT(DDR, port)	&= ~(1<<pin)

#endif /* BITWISE_H */