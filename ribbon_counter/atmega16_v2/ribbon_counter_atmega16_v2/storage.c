/*
 * storage.c
 *
 * Created: 3/24/2015 10:17:20 PM
 *  Author: Vladimir
 */

#include "storage.h"

 // Global variables stored in EEPROM
 uint32_t max_motor_speed_eeprom_var EEMEM = 100; // 0 - max speed, 255 - stop motor
 float diametr_correction_mm_eeprom_var EEMEM = 0.7;
 float distance_not_rewinded_ribbon_mm_eeprom_var EEMEM = TYPICAL_DISTANCE_NOT_REWINDED_RIBBON_MM; // must move to some another location
 uint32_t distance_not_rewinded_ribbon_enable_eeprom_var EEMEM = 1;
 
 void storage_motor_set_speed_limit(uint32_t max_motor_speed) {
	 eeprom_write_dword(&max_motor_speed_eeprom_var, max_motor_speed);
 }
 uint32_t storage_motor_get_speed_limit() {
	 return eeprom_read_dword( &max_motor_speed_eeprom_var );
 }
 
 void storage_set_diametr_correction_mm(float diametr_correction_mm) {
	 eeprom_write_float(&diametr_correction_mm_eeprom_var, diametr_correction_mm);
 }
 float storage_get_diametr_correction_mm() {
	 return eeprom_read_float( &diametr_correction_mm_eeprom_var );
 }
 
 void storage_set_distance_not_rewinded_ribbon_mm(float distance_not_rewinded_ribbon_mm) {
	 eeprom_write_float(&distance_not_rewinded_ribbon_mm_eeprom_var, distance_not_rewinded_ribbon_mm);
 }
 float storage_get_distance_not_rewinded_ribbon_mm() {
	 return eeprom_read_float( &distance_not_rewinded_ribbon_mm_eeprom_var );
 }
 
 void storage_set_distance_not_rewinded_ribbon_enable(uint32_t distance_not_rewinded_ribbon_enable) {
	 eeprom_write_dword(&distance_not_rewinded_ribbon_enable_eeprom_var, distance_not_rewinded_ribbon_enable);
 }
 uint32_t storage_get_distance_not_rewinded_ribbon_enable() {
	 return eeprom_read_dword( &distance_not_rewinded_ribbon_enable_eeprom_var );
 }