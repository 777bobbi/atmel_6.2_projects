#ifndef MOTOR_H
#define MOTOR_H

#include "app.h"

#define MOTOR_PWM_PORT D
#define MOTOR_PWM_PIN PD7 // OC2

#define MOTOR_START_SPEED_IN_PERCENTAGE 12
#define MOTOR_STOP_PWM 255
#define CONVERT_MOTOR_SPEED_PERCENTAGE_TO_PWM(percentage) MOTOR_STOP_PWM - (percentage * MOTOR_STOP_PWM / 100)

// smooth start
#define TIME_INTERVAL_FOR_SPEED_INCREASE_MS 1000

void motor_init();

/*
	0% - stop motor
	100% - max speed
*/
void motor_set_speed_in_percentage(unsigned long);

/*
	return boolean
*/
int motor_is_working();

/*
	unsigned long	- percentage (0...100)
	int				-  time_interval_for_speed_increase_ms (speed increase every ms)
*/
void motor_set_speed_in_percentage_smooth_increase(unsigned long, int);

/*
	Put this handler in main loop
	unsigned long - current_millis (millis())
*/
void motor_handler_smooth_speed_increase(unsigned long);

/*
	Save maximum motor speed limit.
*/
void motor_set_speed_limit_in_percentage();
int motor_get_speed_limit();

#endif
