/*
l74hc165 lib 0x01

copyright (c) Davide Gironi, 2011

Released under GPLv3.
Please refer to LICENSE file for licensing information.
*/


#include <stdio.h>
#include <avr/io.h>
#include <string.h>
#include <util/delay.h>
//#include <lib/uart/uart.h> // for debug

#include "l74hc165.h"

/*
 * init the shift register
 */
void l74hc165_init() {
	
	//output
	L74HC165_DDR |= (1 << L74HC165_CLOCKPIN);
	L74HC165_DDR |= (1 << L74HC165_LOADPIN);
	
	//input
	L74HC165_DDR &= ~(1 << L74HC165_DATAPIN);
	
	//low
	L74HC165_PORT &= ~(1 << L74HC165_LOADPIN);
	L74HC165_PORT &= ~(1 << L74HC165_CLOCKPIN);
	
}

/*
	freeze state
*/
void l74hc165_load(){
	L74HC165_PORT &= ~(1 << L74HC165_LOADPIN);
	L74HC165_PORT |= (1 << L74HC165_LOADPIN);
}

/*
	shift byte
*/
void l74hc165_clock() {
	L74HC165_PORT &= ~(1 << L74HC165_CLOCKPIN);
	L74HC165_PORT |= (1 << L74HC165_CLOCKPIN);
}


/*
 * shift in data
 */
void l74hc165_read(uint8_t *bytearray) {
	l74hc165_load();
	
	for(uint8_t i = 0; i < L74HC165_ICNUMBER; i++) {
		//iterate through the bits in each registers
		uint8_t currentbyte = 0;
		
		for(uint8_t j = 0; j < 8; j++) {
			currentbyte |= ( (L74HC165_PIN & (1 << L74HC165_DATAPIN))>>L74HC165_DATAPIN ) << (7-j);
			l74hc165_clock();
		}
		
		memcpy(&bytearray[i], &currentbyte, 1);
	}
}

	
int l74hc165_read_byte() {
		
	l74hc165_load();
		
	int data = 0;
	char buf[20];
		
	for (int i=0; i<16; i++) {
		
		if ( bit_is_clear(L74HC165_PIN, L74HC165_DATAPIN) ) {
			sprintf(buf, "0");
			uart_puts(buf);
		} else {
			sprintf(buf, "1");
			uart_puts(buf);
		}
		
		
		
		//data |= ( (L74HC165_PIN & (1 << L74HC165_DATAPIN))>>L74HC165_DATAPIN ) << (7-i);
		
		
		//data |= digitalRead(_DATA_Pin)<<(7-i);
		l74hc165_clock();
	}
	
	sprintf(buf, "\n");
	uart_puts(buf);
	
		
	return data;
}
	