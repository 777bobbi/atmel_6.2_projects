#include "foot_pedal.h"

int foot_pedal_pressed_time_ms = 0;
int foot_pedal_is_unpressed = 1;
int foot_pedal_was_pressed = 0;

int earlier_time_foot_pedal_pressed_time_ms = 0;
int stop_propogation = 0; // don't execute onclick event if onkeypress was executed
int motor_speed_in_percentage = MOTOR_START_SPEED_IN_PERCENTAGE;

void foot_pedal_init() {
	IN(FOOT_PEDAL_PORT, FOOT_PEDAL_PIN);
	SET(FOOT_PEDAL_PORT, FOOT_PEDAL_PIN);
}

/*
	Work flow:
	first foot pedal press - motor start;
	every next foot pedal press with delay 1 sec - speed increase:
	1-st pressing	- 25% of max speed
	2-d pressing	- 50% of max speed
	3-rd pressing	- 75% of max speed
	4-th pressing	- max speed
	Sharply press foot pedal - motor stop;
	
	I have concrete button and for every concrete button invokes all next events:
	Click Events:
		onclick - When button is clicked
		ondblclick - When a text is double-clicked
	Input Events:
		onkeypress - When a user is pressing/holding down a key
		onkeyup - When the user releases a key
	Two state:
		- button pressed
		- button pressed with delay
	
	Test case #1:
	- button not pressed
	
	Test case #2:
	- button pressed AND contact bounce less then CONTACT_BOUNCE_DELEY constant (normal behaviour)
	
	Test case #3:
	- button unpressed AND contact bounce less then CONTACT_BOUNCE_DELEY constant (normal behaviour)
	
	Test case #4:
	- button pressed AND contact bounce bigger then CONTACT_BOUNCE_DELEY constant
	
	Test case #5:
	- button unpressed AND contact bounce bigger then CONTACT_BOUNCE_DELEY constant
	
*/
void foot_pedal_listener() {
	if ((READ(FOOT_PEDAL_PORT, FOOT_PEDAL_PIN)) == 0) { // foot pedal is pressed the same as bit_is_clear(PINB, BUTTON_PIN)
		foot_pedal_is_unpressed = 0;
		
		if (foot_pedal_pressed_time_ms >= CONTACT_BOUNCE_DELEY) { // validate contact bounce onkeypress
			foot_pedal_was_pressed = 1;
			
			onkeypress();
		}
		
		foot_pedal_pressed_time_ms += CONTACT_BOUNCE_DELEY; // we can usefoot_pedal_pressed_time_ms variable if we need determine time of long key press
		} else {
		foot_pedal_pressed_time_ms = 0;
		
		if (foot_pedal_was_pressed && foot_pedal_is_unpressed) { // validate contact bounce onkeyup if onkeypress has occurred
			foot_pedal_was_pressed = 0;
			
			onclick();
		}
		
		foot_pedal_is_unpressed = 1; // need for validate onkeyup (unpressing the button) contact bounce
	}
}

void onclick() { // motor start/stop
	earlier_time_foot_pedal_pressed_time_ms = 0;
	
	if (stop_propogation) {
		stop_propogation = 0;
		} else {
		if (motor_is_working()) { // stop motor
			motor_speed_in_percentage = MOTOR_START_SPEED_IN_PERCENTAGE; // reset motor speed in earlier 'motor speed increase session' to default start speed
			
			if (is_auto_rewind_enable()) {
				button_rewind_pause();
				return;
			}
			
			motor_set_speed_in_percentage(0);
		} else { // start motor
			motor_set_speed_in_percentage_smooth_increase(MOTOR_START_SPEED_IN_PERCENTAGE, 20);
		}
	}
}

void onkeypress() { // motor speed increase
	if (motor_is_working() && foot_pedal_pressed_time_ms - earlier_time_foot_pedal_pressed_time_ms >= ONE_SECOND) { // speed increase every one second if motor on
		earlier_time_foot_pedal_pressed_time_ms = foot_pedal_pressed_time_ms;
		stop_propogation = 1;
		
		if (motor_speed_in_percentage < motor_get_speed_limit()) {
			motor_speed_in_percentage += 20;
			motor_set_speed_in_percentage_smooth_increase(motor_speed_in_percentage, 20);
		}
	}
}