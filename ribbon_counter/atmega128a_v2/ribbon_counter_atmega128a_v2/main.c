/*
 * ribbon_counter_atmega128a_v2.c
 *
 * Created: 9/10/2013 9:02:29 PM
 *  Author: Vladimir
 */ 

#define F_CPU 8000000UL
//#define UART_BAUD_RATE 9600

#include <stdio.h>	// sprintf
#include <stdlib.h> //itoa
#include <string.h> // strncpy, strstr
#include <math.h> // floor, round
#include <avr/io.h>
#include <avr/iom128a.h>
#include <avr/interrupt.h> // ISR
//#include <util/delay.h> // for debug
#include <lib/l74hc165/l74hc165.h> // don't forget configure (74HC165 shift registers)
#include <lib/lcd/lcd.h> // don't forget configure
//#include <lib/uart/uart.h>

// For Bitvise Operation Simplification Defines
#define CLR(port,pin)	PORT ## port &= ~(1<<pin)
#define SET(port,pin)	PORT ## port |=  (1<<pin)
#define TOGL(port,pin)	PORT ## port ^=  (1<<pin)
#define READ(port,pin)	PIN  ## port &   (1<<pin)
#define OUT(port,pin)	DDR  ## port |=  (1<<pin)
#define IN(port,pin)	DDR  ## port &= ~(1<<pin)

// For metering unit defines
#define RADIUS 15 // mm
#define CORRECTION 1.2 // mm // devise version1 - 0.02mm, verion2 - 1.05mm
#define CIRCLE_LENGTH (2 * 3.14 * RADIUS + CORRECTION)
#define IMPULSES_PER_REVOLUTION 60
#define METERS_PER_PULS ( CIRCLE_LENGTH  / IMPULSES_PER_REVOLUTION  / 1000 )

// For buttons defines
#define CONTACT_BOUNCE_DELEY 25 //ms
#define MIN_TRESHOLD_FIRST_ACTION 10 // times of the polls pressed button. One poll == CONTACT_BOUNCE_DELEY
#define BUTTONS_SEQUENCE_STARTS_FROM_ONE 1
#define BUTTONS_QUANTITY 14
#define BUTTON_RIBBON_LENGTH_1M 1
#define BUTTON_RIBBON_LENGTH_5M 2
#define BUTTON_RIBBON_LENGTH_20M 3
#define BUTTON_RIBBON_LENGTH_1CM 4
#define BUTTON_RIBBON_LENGTH_10CM 5
#define BUTTON_RIBBON_LENGTH_50CM 6
#define BUTTON_REWIND_PAUSE 7
#define BUTTON_REWIND_WHILE_PRESSED 8
#define BUTTON_DIAMETR_DECREMENT 9
#define BUTTON_DIAMETR_INCREMENT 10
#define BUTTON_DISTANCE_DECREMENT 11
#define BUTTON_DISTANCE_INCREMENT 12
#define BUTTON_MAX_RPM_DECREMENT 13
#define BUTTON_MAX_RPM_INCREMENT 14

// For foot pedal defines
#define PIN_ADC 0
#define PIN_PWM 7

// Function prototypes common
void io_init(void);
void external_interrupts_init(void);
void timer1_init(void);
void dump_int(const int);

// Function prototypes metering unit
void impulsesIncrement(void);
void impulsesDecrement(void);
void setLengthStr(char *destination, double length);
double absf(double n);
void show_count_results(void);

// Function prototypes buttons
void buttons_init();
void handle_buttons();
int poll_buttons();
void button_ribbon_length_1m();
void button_ribbon_length_5m();
void button_ribbon_length_20m();
void button_ribbon_length_1cm();
void button_ribbon_length_10cm();
void button_ribbon_length_50cm();
void button_rewind_pause();
void button_rewind_while_pressed();
void button_diametr_decrement();
void button_diametr_increment();
void button_distance_decrement();
void button_distance_increment();
void button_max_rpm_decrement();
void button_max_rpm_increment();
void button_unpressed();

//Function prototypes foot pedal
void init_adc(void);
uint16_t read_adc(uint8_t channel);
void init_pwm (void);
void set_pwm(int val);
void handle_foot_pedal();

// Global variables metering unit
long long int impulses = 0;
int direction = 1; // 1 - right, -1 - left

// Global variables buttons
uint8_t get_icarray[L74HC165_ICNUMBER];
volatile unsigned long milis = 0;    // This is our shared volatile variable that will be used as the millisecond count
volatile unsigned int button_pressed = 0; // current pressed button
volatile unsigned int button_pressed_polls = 0; // times poll pressed button
void (*buttons_service_routines[BUTTONS_QUANTITY+BUTTONS_SEQUENCE_STARTS_FROM_ONE])(void); // Array of Function Pointers (pt2Function)

int main(void) {
	io_init();
	//external_interrupts_init();
	timer1_init();
	//buttons_init();
	//l74hc165_init();
	//init_adc();
	//init_pwm();
	//uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	sei();	// Global enable interrupts
	lcd_init(LCD_DISP_ON);
	
	lcd_clrscr();
	lcd_puts("Test...");
	lcd_gotoxy(0, 1);
	lcd_puts("Qwerty...");
	
	int blink_count = 0;
	char buff[10];
	//OUT(A,1);
	//CLR(A,1);
	//SET(A,1);
	
	for (;;) {
		if ( milis >= 500 ) { // CONTACT_BOUNCE_DELEY
			milis = 0;
			
			lcd_clrscr();
			itoa(blink_count, buff, 10);
			lcd_puts(buff);
						
			blink_count += 1;
			
			//handle_buttons();
			//handle_foot_pedal();
			//show_count_results();
		}
	}
	
	return 0;
}

void io_init() {
	// LCD
	OUT(D,0); OUT(D,1); OUT(D,2); OUT(D,3); // D4 - D5 - D6 - D7
	OUT(D,4); OUT(D,5); OUT(D,6);	// RS - RW - E
	
	// Buttons
	// Keyboard defines stored in l74hc165.h
	
	// Hall sensors
	//IN(D,0); IN(D,1); IN(D,4); // INT0 - INT1 - plain input
	
	// Foot pedal
	//IN(F,PIN_ADC); // ADC0
	//OUT(B,PIN_PWM); // OC2 PWM to motor (driver)
	
	// LEDs
}

void external_interrupts_init(void) {
	//INT0_vect
	EICRA |= 1<<ISC01|1<<ISC00;	// Trigger INT0 on rising edge.
	EIMSK |= 1<<INT0;			// Enable interrupt INT0
	EIFR |= 1<<INT0;			// Cleared flag by writing a logical one to it. This scaffolding need to avoid first interrupt occur, because interrupt pin set pull up.
	
	//INT1_vect
	EICRA |= 1<<ISC11;	// Trigger INT1 on falling edge.
	EIMSK |= 1<<INT1;	// Enable interrupt INT1
	EIFR |= 1<<INT1;	// Cleared flag by writing a logical one to it. This scaffolding need to avoid first interrupt occur, because interrupt pin set pull up.
}

/*
	Trigger interrupt every one ms
*/
void timer1_init(void) {
	TCCR1B	|= 1<<WGM12;			//Timer in CTC mode
	TCCR1B	|= 1<<CS11|1<<CS10;		//Prescaler to 1:64/F_CPU:64
	OCR1A	= 124;					//Value to have an compare at every 1ms = 1000Hz (Target Timer Count = (((Input Frequency / Prescaler) / Target Frequency) - 1) -> Target Timer Count = (((8000000Hz / 64) / 1000Hz) - 1) = 124)
	TIMSK	= 1<<OCIE1A;			//Enable timer interrupts
}

void buttons_init() {
	buttons_service_routines[BUTTON_RIBBON_LENGTH_1M] = button_ribbon_length_1m;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_5M] = button_ribbon_length_5m;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_20M] = button_ribbon_length_20m;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_1CM] = button_ribbon_length_1cm;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_10CM] = button_ribbon_length_10cm;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_50CM] = button_ribbon_length_50cm;
	buttons_service_routines[BUTTON_REWIND_PAUSE] = button_rewind_pause;
	buttons_service_routines[BUTTON_REWIND_WHILE_PRESSED] = button_rewind_while_pressed;
	buttons_service_routines[BUTTON_DIAMETR_DECREMENT] = button_diametr_decrement;
	buttons_service_routines[BUTTON_DIAMETR_INCREMENT] = button_diametr_increment;
	buttons_service_routines[BUTTON_DISTANCE_DECREMENT] = button_distance_decrement;
	buttons_service_routines[BUTTON_DISTANCE_INCREMENT] = button_distance_increment;
	buttons_service_routines[BUTTON_MAX_RPM_DECREMENT] = button_max_rpm_decrement;
	buttons_service_routines[BUTTON_MAX_RPM_INCREMENT] = button_max_rpm_increment;
}


void init_adc(void) {
	ADCSRA |= ((1<<ADPS2)|(1<<ADPS1));  // (ADCSRA - ADC Control and Status Register A) 8Mhz/64 = 125Khz the ADC reference clock
	ADMUX |= (1<<REFS0);                // (ADC Multiplexer Selection Register) Voltage reference from Avcc (5v) (Don't forgot to connect Vcc to AREF pin - the threshold level of voltage. )
	ADCSRA |= (1<<ADEN);                // Turn on ADC
	ADCSRA |= (1<<ADSC);                // Do an initial conversion because this one is the slowest and to ensure that everything is up and running
}

uint16_t read_adc(uint8_t channel) {
	ADMUX &= 0xF0;                   // (0b1111 0000) Clear the older channel that was read
	ADMUX |= channel;                // Defines the new ADC channel to be read
	ADCSRA |= (1<<ADSC);             // Starts a new conversion
	while(ADCSRA & (1<<ADSC));       // (0b0100 0000 & (0b0100 0000)) Wait until the conversion is done. (In Single Conversion mode, write bit ADSC to one to start each conversion.)
	return ADCW;                   	 // Returns the ADC value of the chosen channe
}

/*
 start timer:
 WGM20			- set 'PWM, Phase Correct' mode
 COM21, COM20	- set OC2 on compare match when up-counting. Clear OC2 on compare match when down-counting.
 CS20			- clkI/O (No prescaling)
 CS21, CS20		- clkI/64 = 125 KHz (From prescaler)
 CS22			- clkI/256 = 31250 Hz (From prescaler)
 CS22, CS20		- clkI/1024 = 7812 Hz (From prescaler)
*/
void init_pwm (void)
{
	TCCR2 = 1<<WGM20|1<<COM21|1<<COM20|1<<CS20;
	OCR2 = 0; // initial value on PWM pin output
}

void set_pwm(int val) {
	OCR2 = val;
}

/*
	External Interrupt service routine
	Configured as rising edge
*/
ISR(INT0_vect) {
	if ( READ(D, 1) ) {
		impulsesIncrement();
	} else {
		impulsesDecrement();
	}
}

/*
	External Interrupt service routine
	Configured as falling edge
*/
ISR(INT1_vect) {
	if ( READ(D, 1) ) {
		impulsesDecrement();
	} else {
		impulsesIncrement();
	}
}

/*
	Internal interrupt service routine
	Increment milis quantity every one millisecond
*/
ISR(TIMER1_COMPA_vect) {
	milis += 1;
}

void show_count_results() {
	//char lengthStr[16];
	//double length;
	//char directionLeft[4] = "<<<";
	//char directionRight[4] = ">>>";
	//char buff[16];
	
	//lcd_clrscr();
	//length = absf(METERS_PER_PULS * impulses);
	//setLengthStr(lengthStr, length);
	//lcd_puts(lengthStr);
	//
	//lcd_gotoxy(0, 1);
	//if ( direction == 1 ) {
	//lcd_puts(directionRight);
	//} else {
	//lcd_puts(directionLeft);
	//}
	//
	//lcd_gotoxy(5, 1);
	//sprintf(buff, "%d", impulses);
	//lcd_puts(buff);
}

void impulsesIncrement() {
	impulses += 1;
	direction = 1;
}

void impulsesDecrement() {
	impulses -= 1;
	direction = -1;
}

double absf(double n) {
	if ( n < 0 ) {
		return n * -1;
	}
	return n;
}

void setLengthStr(char *destination, double length) {
	char centimeters[3];
	char millimeters[2];
	char * pch;
	int position;
	
	centimeters[2] = 0;
	millimeters[1] = 0;
	
	sprintf(destination, "%.3f", length);
	
	pch = strstr(destination, ".");
	position = pch - destination + 1;

	strncpy(centimeters, destination + position, 2);
	strncpy(millimeters, destination + position + 2, 1);
	
	sprintf(destination, "%.0fm %scm %smm\n", floor(length), centimeters, millimeters);
}

void handle_foot_pedal() {
	int pwm_value = 0;
	
	pwm_value = (int)round(read_adc(PIN_ADC)/4);
	set_pwm(pwm_value);
}

/*
	every CONTACT_BOUNCE_DELEY ms poll buttons
	for every pressed button call according handler function
*/
void handle_buttons() {
	int current_button_pressed = poll_buttons();
	
	if ( current_button_pressed == button_pressed && button_pressed != 0 ) { //contact bounce handle
		button_pressed_polls += 1;
		
		if ( button_pressed_polls >= MIN_TRESHOLD_FIRST_ACTION ) { //execute button service routine
			dump_int(current_button_pressed);
			buttons_service_routines[current_button_pressed]();
		}
	} else if ( current_button_pressed ) { //first button poll
		button_pressed = current_button_pressed;
		button_pressed_polls = 1;
	} else { //buttons unpressed
		button_pressed = 0;
		button_pressed_polls = 0;
		button_unpressed();
	}
}

/*
	poll buttons 
	return: current pressed button id starts from less bit or 0 if no one button was pressed
*/
int poll_buttons() {
	uint8_t bytearray[L74HC165_ICNUMBER];
	const uint8_t pressed = 0; // 0 - button pressed, 1 - button unpressed
	int button_pressed_number = 1;
	
	l74hc165_shiftin(bytearray); //read data
	
	for(int i = 0; i < L74HC165_ICNUMBER; i++) {
		for (int button = 1; button <= 128; button <<= 1, button_pressed_number++) {
			if ( (button & bytearray[i]) == pressed && button_pressed_number <= BUTTONS_QUANTITY ) {
				return button_pressed_number;
			}
		}
	}
	
	return 0;
}

void button_ribbon_length_1m() {

}
void button_ribbon_length_5m() {
	
}
void button_ribbon_length_20m() {

}
void button_ribbon_length_1cm() {
	
}
void button_ribbon_length_10cm() {
	
}
void button_ribbon_length_50cm() {
	
}
void button_rewind_pause() {
	
}
void button_rewind_while_pressed() {
	
}
void button_diametr_decrement() {
	
}
void button_diametr_increment() {
	
}
void button_distance_decrement() {
	
}
void button_distance_increment() {
	
}
void button_max_rpm_decrement() {
	
}
void button_max_rpm_increment() {

}

void button_unpressed() {
	lcd_clrscr();
}

void dump_int(const int n) {
	char buf[20];
	
	//memset(buf, 0, sizeof(buf)); // clear buffer and nulled last byte
	
	sprintf(buf, "%d ", n);
	lcd_clrscr();
	lcd_puts(buf);
	
	//uart_puts(buf);
}