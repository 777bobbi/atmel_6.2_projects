/*
 *
 * Created: 5/7/2014 4:35:32 PM
 *  Author: Vladimir
 *
 * http://hekilledmywire.wordpress.com/2011/05/28/introduction-to-timers-tutorial-part-6/
 *
 
 var inputFrequencyHZ = 16000000;
 var prescaler = 1;
 var targetFrequencyHz = 1000000;
 var getTimerResolution = function(inputFrequencyHZ, prescaler, targetFrequencyHz) {
	 var timerResolution = (((inputFrequencyHZ / prescaler) / targetFrequencyHz) - 1);
	 console.log("Timer resolution: "+timerResolution);
 }
 getTimerResolution(inputFrequencyHZ, prescaler, targetFrequencyHz);
 
 */

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

#define PIN_LED PD4

//function prototype
void io_init(void);		
void timercounter0_8bit_init(void);
void timercounter1_16bit_init(void);
void timercounter2_8bit_init(void);

//global variables
volatile unsigned long microseconds = 0;
volatile unsigned long milliseconds = 0;
unsigned long int now = 0;

int main(void){
	io_init();
	timercounter1_16bit_init();
	timercounter2_8bit_init();
	sei(); //Enable global interrupts
	
	while(1){
		if(microseconds >= 5000){
			microseconds = 0;
			PORTD ^= 1<<PIN_LED;    //Toggle the led state every 500ms/0.5s/2hz
		}
	}
	
	return 0;
}

void io_init(void) {
	DDRD |= 1<<PIN_LED; //Set as output
	PORTD &= ~(1<<PIN_LED); // 0 - led off, 1 - led on
}

//Execute interrupt every one millisecond
void timercounter1_16bit_init(void){
	TCCR1B |= 1<<WGM12; //Configure timer in CTC mode
	TCCR1B |= 1<<CS10; //Set prescaler to 1:1 (F_CPU:1) and this action enable timer
	OCR1A = 15999; //Value to have an compare at every 1ms = 1000Hz (Target Timer Count = (((Input Frequency / Prescaler) / Target Frequency) - 1) -> Target Timer Count = (((16000000Hz / 1) / 1000Hz) - 1) = 15999)
	TIMSK |= 1<<OCIE1A; //Enable timer interrupts
}

//Execute interrupt every one hundred microseconds
void timercounter2_8bit_init(void){
	TCCR2 |= 1<<WGM21; //Configure timer in CTC mode
	TCCR2 |= 1<<CS22; //Set prescaler to 1:64 (F_CPU:64) and this action enable timer
	OCR2 = 24; //Value to have an compare at every 1us = 1000000Hz (Target Timer Count = (((Input Frequency / Prescaler) / Target Frequency) - 1) -> Target Timer Count = (((16000000Hz / 1) / 1000000Hz) - 1) = 15)
	TIMSK |= 1<<OCIE2; //Enable timer interrupts
}

//This interrupt service routine executes every one millisecond
ISR(TIMER1_COMPA_vect){
	milliseconds++;
}

//This interrupt service routine executes every one microsecond
ISR(TIMER2_COMP_vect) {
	microseconds++;
}