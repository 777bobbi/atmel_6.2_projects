/*
 * inerrapts_external.c
 *
 * Created: 8/29/2013 2:53:14 PM
 *  Author: Vladimir
 
 Things to note:
 1 - The "avr/interrupt.h" files must be included to use interrupts.

 2 - The ISR() function is a general interrupt service routine for all interrupts. 
 This means that you can have several ISR() function in an AVR C program. 
 The argument "INT0_vect" indicate that this ISR is for the External Interrupt 0.
 
 interrapt.h - http://www.nongnu.org/avr-libc/user-manual/group__avr__interrupts.html
 
 */ 

#define F_CPU 8000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/iom128a.h> //mega128a
#include <util/delay.h>

void io_init(void);
void external_interrupts_init(void);

int main(void)
{
	io_init();
	external_interrupts_init();
	sei(); //Enable Global Interrupt
	
	for(;;);
	
	return 0;
}

void io_init(void) {
	DDRA = 0xFF; // data direction register - set all output (diodes)
	DDRD |= 1<<DDRD0|1<<DDRD1|1<<DDRD2|1<<DDRD3; // data direction register. Set as input (interrupts buttons)
		
	PORTA = 0b11101110; //0 - led on, 1 - led off
	PORTD = 0x0f; //Pull up buttons because they closing on ground
}

void external_interrupts_init(void) {
	//INT0_vect
	//EIMSK &= ~(1<<INT0); // !!! When changing the ISCn1/ISCn0 bits, the interrupt must be disabled by clearing its Interrupt Enable bit in the EIMSK Register. Otherwise an interrupt can occur when the bits are changed.
	EICRA |= 1<<ISC01|1<<ISC00;	// Trigger INT0 on rising edge.
	EIMSK |= 1<<INT0; // Enable interrupt INT0
	EIFR |= 1<<INT0; // Cleared flag by writing a logical one to it. This scaffolding need to avoid first interrupt occur, because interrupt pin set pull up.
	
	//INT1_vect
	EICRA |= 1<<ISC11;	// Trigger INT1 on falling edge.
	EIMSK |= 1<<INT1; // Enable interrupt INT1
	EIFR |= 1<<INT1; // Cleared flag by writing a logical one to it. This scaffolding need to avoid first interrupt occur, because interrupt pin set pull up.
}

//Interrupt Service Routine for INT0
ISR(INT0_vect)
{
	PORTA = ~PORTA; // invert port
	_delay_ms(1000);
	PORTA = ~PORTA; // invert port back
}


//Interrupt Service Routine for INT0
ISR(INT1_vect)
{
	PORTA = 0x00; // led on
	_delay_ms(1000);
	PORTA = 0b11101110;
}