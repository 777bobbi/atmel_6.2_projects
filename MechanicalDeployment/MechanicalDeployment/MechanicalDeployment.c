/*
 * MechanicalDeployment.c
 *
 * Created: 20.05.2013 20:59:38
 *  Author: Vladimir
 */ 

#define F_CPU 8000000UL

#include <avr/io.h>
#include <util/delay.h>

void delay(int us) {
	for (int j = 0; j < us; j++  ) {
		_delay_us(1);
	}
}

int main(void)
{
    int size = 11;
	int us = 1700;
	 
	DDRA = 0xFF;
	PORTA = 0xFF; // pull-up - all off
	
	DDRD = 0x00;
	PORTD = 0xFF;
	
	int mecanicalDeployment[11] = {
		0b11111111,
		0b11111111,
		0b10000001,
		0b01111110,
		0b01111110,
		0b01100110,
		0b01111110,
		0b01111110,
		0b10000001,
		0b11111111,
		0b11111111,
	};
	
	//int mecanicalDeployment[35] = {
		//0b11111111,
		//0b11111111,
		//0b11000000,
		//0b01111110,
		//0b01111110,
		//0b10000000,
		//0b01111110,
		//0b01111110,
		//0b10000000,
		//0b11111111,
		//0b11111111,
		//0b10000001,
		//0b01111110,
		//0b01111110,
		//0b01111110,
		//0b10000001,
		//0b11111111,
		//0b11111111,
		//0b11000000,
		//0b01111110,
		//0b01111110,
		//0b10000000,
		//0b01111110,
		//0b01111110,
		//0b10000000,
		//0b11111111,
		//0b11111111,
		//0b11100111,
		//0b11011011,
		//0b10111101,
		//0b00000000,
		//0b01111110,
		//0b01111110,
		//0b11111111,
		//0b11111111,
	//};



	while(1)
	{
		if ( bit_is_clear(PIND, 0) ) {
			_delay_ms(30);
			if ( bit_is_clear(PIND, 0) ) {
				us += 100;
			}

		}
		if ( bit_is_clear(PIND, 1) ) {
			_delay_ms(30);
			if ( bit_is_clear(PIND, 1) ) {
				us -= 100;
			}

		}
		for (int i = 0; i < size; i++ ) {
			PORTA = mecanicalDeployment[i];
			delay(us);	
		}
	}
	
	return 0;
}