#ifndef __INIT_H__
#define __INIT_H__

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

#define LED_RED PD4

class Init
{
public:
	void interrupts();
	void io();
};

#endif
