#ifndef __SYSTEM_H__
#define __SYSTEM_H__


class System
{
public:
	unsigned long millis(void);
	unsigned long micros(void);
};

#endif
