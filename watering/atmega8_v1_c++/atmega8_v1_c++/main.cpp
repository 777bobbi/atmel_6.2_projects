/*
 * atmega8_v1_c__.cpp
 *
 * Created: 5/20/2014 8:15:05 AM
 *  Author: Vladimir
 */ 

#include "Controller/App.h"

int main(void)
{
	App app;
	
	app.setup();
	
    while(1)
    {
        app.loop();
    }
}