#ifndef __BASE_H__
#define __BASE_H__

#include "../Model/Init.h"
#include "../Model/System.h"

class App
{
	public:
		void setup();
		void loop();
};

#endif
