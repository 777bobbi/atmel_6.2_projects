/*
 * keyboard_74HC165_buttons_handle.c
 *
 * Created: 9/10/2013 4:08:05 PM
 *  Author: Vladimir
 
 How to call the function using function pointer - http://stackoverflow.com/questions/1952175/how-to-call-the-function-using-function-pointer
 
 //itoa (bytearray[i], uart_str, 2);
 //uart_puts(strcat(uart_str, " "));
 //sprintf(uart_str, "%d ", button_pressed_number);
 uart_puts(uart_str);
 char uart_str[20] = "\0"; // bite + " " + \0
 char uart_str[10];
 uart_str[0] = '\0';
 
 */ 

#define F_CPU 8000000UL
#define UART_BAUD_RATE 9600
#define PIN_LED PORTA1
#define CONTACT_BOUNCE_DELEY 25 //ms
#define MIN_TRESHOLD_FIRST_ACTION 10 // times of the polls pressed button. One poll == CONTACT_BOUNCE_DELEY

#define BUTTONS_QUANTITY 14
#define RESERVED_BUTTON 1
#define BUTTON_RIBBON_LENGTH_1M 1
#define BUTTON_RIBBON_LENGTH_5M 2
#define BUTTON_RIBBON_LENGTH_20M 3
#define BUTTON_RIBBON_LENGTH_1CM 4
#define BUTTON_RIBBON_LENGTH_10CM 5
#define BUTTON_RIBBON_LENGTH_50CM 6
#define BUTTON_REWIND_PAUSE 7
#define BUTTON_REWIND_WHILE_PRESSED 8
#define BUTTON_DIAMETR_DECREMENT 9
#define BUTTON_DIAMETR_INCREMENT 10
#define BUTTON_DISTANCE_DECREMENT 11
#define BUTTON_DISTANCE_INCREMENT 12
#define BUTTON_MAX_RPM_DECREMENT 13
#define BUTTON_MAX_RPM_INCREMENT 14

#include <stdio.h>		// sprintf
#include <stdlib.h>		// itoa
#include <string.h>     // strcat
#include <avr/io.h>
#include <avr/iom128a.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <uart/uart.h>
#include <l74hc165/l74hc165.h>

uint8_t get_icarray[L74HC165_ICNUMBER];
volatile unsigned long milis = 0;    //This is our shared volatile variable that will be used as the millisecond count
volatile unsigned int button_pressed = 0; // current pressed button
volatile unsigned int button_pressed_polls = 0; // times poll pressed button
void (*buttons_service_routines[BUTTONS_QUANTITY+RESERVED_BUTTON])(void); //Array of Function Pointers (pt2Function)

//Functions prototype
void io_init(void);		
void timer1_init(void);
void buttons_init();
void handle_buttons();
int poll_buttons();
void button_ribbon_length_1m_handler();
void button_ribbon_length_5m_handler();
void button_ribbon_length_20m_handler();
void button_ribbon_length_1cm_handler();
void button_ribbon_length_10cm_handler();
void button_ribbon_length_50cm_handler();
void button_rewind_pause();
void button_rewind_while_pressed();
void button_diametr_decrement();
void button_diametr_increment();
void button_distance_decrement();
void button_distance_increment();
void button_max_rpm_decrement();
void button_max_rpm_increment();

int main(void)
{
	io_init();
	timer1_init();
	buttons_init();
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	l74hc165_init();
	sei(); //Enable global interrupts (UART, ...)
	
	for(;;) {
		
		handle_buttons();
		
	}

}

void io_init(void) {
	DDRA	|= 1<<PIN_LED; //Set as output
	PORTA	&= ~(1<<PIN_LED); // 1 - led off, 0 - led on, Proteus simulator - backwards
}

void timer1_init(void){
	TCCR1B	|= 1<<WGM12;			//Timer in CTC mode
	TCCR1B	|= 1<<CS11|1<<CS10;		//Prescaler to 1:64/F_CPU:64
	OCR1A	= 124;					//Value to have an compare at every 1ms = 1000Hz (Target Timer Count = (((Input Frequency / Prescaler) / Target Frequency) - 1) -> Target Timer Count = (((8000000Hz / 64) / 1000Hz) - 1) = 124)
	TIMSK	= 1<<OCIE1A;			//Enable timer interrupts
}

void buttons_init() {
	buttons_service_routines[BUTTON_RIBBON_LENGTH_1M] = button_ribbon_length_1m_handler;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_5M] = button_ribbon_length_5m_handler;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_20M] = button_ribbon_length_20m_handler;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_1CM] = button_ribbon_length_1cm_handler;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_10CM] = button_ribbon_length_10cm_handler;
	buttons_service_routines[BUTTON_RIBBON_LENGTH_50CM] = button_ribbon_length_50cm_handler;
	buttons_service_routines[BUTTON_REWIND_PAUSE] = button_rewind_pause;
	buttons_service_routines[BUTTON_REWIND_WHILE_PRESSED] = button_rewind_while_pressed;
	buttons_service_routines[BUTTON_DIAMETR_DECREMENT] = button_diametr_decrement;
	buttons_service_routines[BUTTON_DIAMETR_INCREMENT] = button_diametr_increment;
	buttons_service_routines[BUTTON_DISTANCE_DECREMENT] = button_distance_decrement;
	buttons_service_routines[BUTTON_DISTANCE_INCREMENT] = button_distance_increment;
	buttons_service_routines[BUTTON_MAX_RPM_DECREMENT] = button_max_rpm_decrement;
	buttons_service_routines[BUTTON_MAX_RPM_INCREMENT] = button_max_rpm_increment;
}

ISR(TIMER1_COMPA_vect){ //This is our interrupt service routine
	milis += 1; //Increase milis count by one millisecond
}

/*
	every 25 ms poll buttons
	for every pressed button call according handler function
*/
void handle_buttons() {
	if ( milis < CONTACT_BOUNCE_DELEY ) {
		return;
	}
	
	milis = 0;
	int current_button_pressed = poll_buttons();
	
	if ( current_button_pressed == button_pressed && button_pressed != 0 ) { //contact bounce handle
		button_pressed_polls += 1;
		
		if ( button_pressed_polls >= MIN_TRESHOLD_FIRST_ACTION ) { //execute button service routine
			buttons_service_routines[current_button_pressed]();
		}
	} else if ( current_button_pressed ) { //first button poll
		button_pressed = current_button_pressed;
		button_pressed_polls = 1;
	} else { //buttons unpressed
		button_pressed = 0;
		button_pressed_polls = 0;
		PORTA &= ~(1<<PIN_LED);
	}
}

/*
	poll buttons 
	return: current pressed button id starts from less bit or 0 if no one button was pressed
*/
int poll_buttons() {
	uint8_t bytearray[L74HC165_ICNUMBER];
	const uint8_t pressed = 0; // 0 - button pressed, 1 - button unpressed
	int button_pressed_number = 1;
	
	l74hc165_shiftin(bytearray); //read data
	
	for(int i = 0; i < L74HC165_ICNUMBER; i++) {
		for (int button = 1; button <= 128; button <<= 1, button_pressed_number++) {
			if ( (button & bytearray[i]) == pressed && button_pressed_number <= BUTTONS_QUANTITY ) {
				return button_pressed_number;
			}
		}
	}
	
	return 0;
}

void button_ribbon_length_1m_handler() {
	PORTA |= 1<<PIN_LED;
}
void button_ribbon_length_5m_handler() {
	
}
void button_ribbon_length_20m_handler() {
	PORTA |= 1<<PIN_LED;
}
void button_ribbon_length_1cm_handler() {
	
}
void button_ribbon_length_10cm_handler() {
	
}
void button_ribbon_length_50cm_handler() {
	
}
void button_rewind_pause() {
	
}
void button_rewind_while_pressed() {
	
}
void button_diametr_decrement() {
	
}
void button_diametr_increment() {
	
}
void button_distance_decrement() {
	
}
void button_distance_increment() {
	
}
void button_max_rpm_decrement() {
	
}
void button_max_rpm_increment() {
	PORTA |= 1<<PIN_LED;
}