/*
 * uart_test4.c
 *
 * Created: 8/28/2013 4:55:51 PM
 *  Author: Vladimir
 
 example http://www.avrfreaks.net/index.php?name=PNphpBB2&file=printview&t=57221&start=0
 lib ref http://homepage.hispeed.ch/peterfleury/group__pfleury__uart.html
 
 */ 

#define F_CPU 8000000UL
#define UART_BAUD_RATE 9600

#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/iom128a.h>
#include <util/delay.h>
#include <uart.h>


char string[] = "qwerty!!!\n";

int main(void)
{
    uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	/* 
     * now enable interrupt, since UART library is interrupt controlled 
     */ 
    sei();

	while(1)
    {
        uart_puts(string);
		_delay_ms(50); 
    }
	
	return 0;
}