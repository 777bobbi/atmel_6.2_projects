/*
 * adc_interrupt.c
 *
 * Created: 9/2/2013 10:31:08 AM
 *  Author: Vladimir
 */ 

#define F_CPU 8000000UL
//#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit)) //Sets pin LOW: 0 volts  - sbi(PORTB, PB1);
//#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit) //Sets pin HIGH: 5 volts

#define PIN_ADC DDRF0

#include <stdlib.h>
#include <avr/io.h>
#include <avr/iom128a.h>
#include <avr/interrupt.h>

/*ADC Conversion Complete Interrupt Service Routine (ISR)*/
ISR(ADC_vect)
{
	PORTA = (int)(ADC/4);	// Output to PortA. ADC - the result of adc conversio; top limit -1024.
	ADCSRA |= 1<<ADSC;	// Start Conversion
}

void io_init(void) {
	 DDRA = 0xFF;	// Configure PortA as output
	 DDRF &= ~(1 << PIN_ADC); // set pin ADC like input.
	 
	 PORTA = 0xFF;	// 0 - leds on, 1 - leds off
}

void adc_init(void) {
	ADCSRA |= 1<<ADPS2 | 1<<ADPS1;	// (ADCSRA - ADC Control and Status Register A) 8Mhz/64 = 125Khz the ADC reference clock
	ADMUX  |= 1<<REFS0;                // (ADC Multiplexer Selection Register) Voltage reference from Avcc (5v)
	ADCSRA |= 1<<ADEN;                // Turn on ADC
	ADCSRA |= 1<<ADIE;				// Enable ADC Interrupt !!!
	ADCSRA |= 1<<ADSC;                // Start conversion. Do an initial conversion because this one is the slowest and to ensure that everything is up and running
}

int main(void)
{
	io_init();	
	adc_init();

	sei();				// Enable Global Interrupts
	
	while(1);
}