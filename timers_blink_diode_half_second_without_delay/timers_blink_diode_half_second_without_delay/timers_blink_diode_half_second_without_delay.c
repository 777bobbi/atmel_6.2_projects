/*
 * timers_blink_diode_half_second_without_delay.c
 *
 * Created: 9/9/2013 6:30:10 PM
 *  Author: Vladimir
 
 http://hekilledmywire.wordpress.com/2011/05/28/introduction-to-timers-tutorial-part-6/
 
 
 */ 


#define F_CPU 8000000UL
#define PIN_LED PORTA1

#include <avr/io.h>
#include <avr/iom128a.h>
#include <avr/interrupt.h>

void io_init(void);		//Function prototype
void timer1_init(void);

volatile unsigned long milis = 0;    //This is our shared volatile variable that will be used as the milis count
unsigned long now = 0;

int main(void){
	io_init();
	timer1_init();
	
	for(;;){
		if(milis - now >= 500){
			now = milis;
			PORTA ^= 1<<PIN_LED;    //Toggle the led state every 500ms/0.5s/2hz
		}
	}
	
	return 0;
}

void io_init(void) {
	DDRA	|= 1<<PIN_LED; //Set as output
	PORTA	|= 1<<PIN_LED; // 1 - led off, 0 - led on
}

void timer1_init(void){
	TCCR1B	|= 1<<WGM12;			//Timer in CTC mode
	TCCR1B	|= 1<<CS11|1<<CS10;		//Prescaler to 1:64/F_CPU:64
	OCR1A	= 124;					//Value to have an compare at every 1ms = 1000Hz (Target Timer Count = (((Input Frequency / Prescaler) / Target Frequency) - 1) -> Target Timer Count = (((8000000Hz / 64) / 1000Hz) - 1) = 124)
	TIMSK	= 1<<OCIE1A;			//Enable timer interrupts
	sei();							//Enable global interrupts
}

ISR(TIMER1_COMPA_vect){        //This is our interrupt service routine
	milis++;    //Increase milis count by one millisecond
}