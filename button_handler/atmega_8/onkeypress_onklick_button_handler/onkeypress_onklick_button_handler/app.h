#ifndef APP_H_
#define APP_H_

#define F_CPU 16000000UL
#define CONTACT_BOUNCE_DELEY 25 //ms

#include <avr/io.h>
#include <avr/interrupt.h>
#include "bitwise.h"


void timer1_init();
unsigned long millis();

#endif /* APP_H_ */