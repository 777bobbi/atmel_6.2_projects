#ifndef BUTTON_H_
#define BUTTON_H_

#include "app.h"
#include "led_blink.h"

#define BUTTON_PORT B
#define BUTTON_PIN PB0
#define ONE_SECOND 1000 // ms
#define ONE_HUNDRED_PERCENT 100 // percentage

#define LED_BLINK_START_SPEED_PERCENT 10

void button_init();
void button_listener();
void onkeypress();
void onclick();

#endif /* BUTTON_H_ */