#include "app.h"

volatile unsigned long timer0_millis = 0;

/*
	Trigger interrupt every one ms
*/
void timer1_init()
{
	TCCR1B	|= 1<<WGM12;			//Timer in CTC mode
	TCCR1B	|= 1<<CS11|1<<CS10;		//Prescaler to 1:64/F_CPU:64
	OCR1A	= 249;					//Value to have an compare at every 1ms = 1000Hz (Target Timer Count = (((Input Frequency / Prescaler) / Target Frequency) - 1) -> Target Timer Count = (((16000000Hz / 64) / 1000Hz) - 1) = 249)
	TIMSK	= 1<<OCIE1A;			//Enable timer interrupts
}

/*
	Internal interrupt service routine
	Increment timer0_millis quantity every one millisecond
*/
ISR(TIMER1_COMPA_vect) {
	timer0_millis += 1;
}

unsigned long millis()
{
	unsigned long m;
	char oldSREG = SREG;
	
	cli(); // disable interrupts while we read timer0_millis or we might get an inconsistent value (e.g. in the middle of a write to timer0_millis)
	m = timer0_millis;
	SREG = oldSREG;

	return m;
}
