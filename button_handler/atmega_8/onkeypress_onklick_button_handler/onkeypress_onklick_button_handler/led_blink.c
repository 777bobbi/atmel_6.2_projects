#include "led_blink.h"

int is_led_blinking = 0;
unsigned long blink_time_interval = LED_BLINK_MIN_SPEED; //ms
unsigned long earlier_time_led_blink = 0;

void led_blink_init() {
	OUT(LED_BLINK_PORT, LED_BLINK_PIN); // data direction register - set led pin as output
	CLR(LED_BLINK_PORT, LED_BLINK_PIN); // set led off
}

void led_blink_handler() {
	if (is_led_blinking && millis() - earlier_time_led_blink >= blink_time_interval) {
		earlier_time_led_blink = millis();
		
		TOGL(LED_BLINK_PORT, LED_BLINK_PIN);
	}
}

void set_led_blink_speed(unsigned long percentage) {
	if ( percentage > 100 ) {
		percentage = 100;
	}
	
	if (percentage <= 0) {
		is_led_blinking = 0;
		led_off();
		return;
	}
	
	led_on();
	earlier_time_led_blink = millis(); // This need in order to first blinking loop was executed totally. 
	is_led_blinking = 1;
	
	blink_time_interval = CONVERT_SPEED_PERCENT_TO_MS(percentage);
}

void led_on() {
	SET(LED_BLINK_PORT, LED_BLINK_PIN);
}

void led_off() {
	CLR(LED_BLINK_PORT, LED_BLINK_PIN);
}

int led_blinking() {
	return is_led_blinking;
}