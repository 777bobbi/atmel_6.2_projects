/*
 * onkeypress_onklick_button_handler.c
 *
 * Created: 3/15/2015 1:49:55 PM
 *  Author: Vladimir
 */ 

#include "app.h"
#include "button.h"
#include "led_blink.h"

int main(void)
{
	unsigned long earlier_time = 0;
	
	timer1_init();
	led_blink_init();
	button_init();
	sei(); //Enable Global Interrupt
	
	while(1)
	{
		if (millis() - earlier_time >= CONTACT_BOUNCE_DELEY) {
			earlier_time = millis();
			
			button_listener();
		}
		
		led_blink_handler();
	}
	
	return 0;
}