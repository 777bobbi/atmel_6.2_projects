#include "button.h"

int earlier_time_foot_pedal_pressed_time_ms = 0;
int foot_pedal_pressed_time_ms = 0;
int foot_pedal_is_unpressed = 1;
int foot_pedal_was_pressed = 0;

int stop_propogation = 0;
int led_blink_speed = LED_BLINK_START_SPEED_PERCENT; // percentage

void button_init() {
	IN(BUTTON_PORT, BUTTON_PIN); // data direction register - set button pin like input
	SET(BUTTON_PORT, BUTTON_PIN); // pull up, set hi level
}

/*
	Work flow:
	first foot pedal press - motor start;
	every next foot pedal press with delay 1 sec - speed increase:
	1-st pressing	- 25% of max speed
	2-d pressing	- 50% of max speed
	3-rd pressing	- 75% of max speed
	4-th pressing	- max speed
	Sharply press foot pedal - motor stop;
	
	I have concrete button and for every concrete button invokes all next events:
	Click Events:
		onclick - When button is clicked
		ondblclick - When a text is double-clicked
	Input Events:
		onkeypress - When a user is pressing/holding down a key
		onkeyup - When the user releases a key
	Two state:
		- button pressed
		- button pressed with delay
	
	Test case #1:
	- button not pressed
	
	Test case #2:
	- button pressed AND contact bounce less then CONTACT_BOUNCE_DELEY constant (normal behaviour)
	
	Test case #3:
	- button unpressed AND contact bounce less then CONTACT_BOUNCE_DELEY constant (normal behaviour)
	
	Test case #4:
	- button pressed AND contact bounce bigger then CONTACT_BOUNCE_DELEY constant
	
	Test case #5:
	- button unpressed AND contact bounce bigger then CONTACT_BOUNCE_DELEY constant
	
*/
void button_listener() {
	
	if ((READ(BUTTON_PORT, BUTTON_PIN)) == 0) { // button is pressed the same as bit_is_clear(PINB, BUTTON_PIN)
		foot_pedal_is_unpressed = 0;
			
		if (foot_pedal_pressed_time_ms >= CONTACT_BOUNCE_DELEY) { // validate contact bounce onkeypress
			foot_pedal_was_pressed = 1;
			
			onkeypress();
		}
		
		foot_pedal_pressed_time_ms += CONTACT_BOUNCE_DELEY; // we can usefoot_pedal_pressed_time_ms variable if we need determine time of long key press
	} else {
		foot_pedal_pressed_time_ms = 0;
		
		if (foot_pedal_was_pressed && foot_pedal_is_unpressed) { // validate contact bounce onkeyup if onkeypress has occurred
			foot_pedal_was_pressed = 0;
			
			onclick();
		}
		
		foot_pedal_is_unpressed = 1; // need for validate onkeyup (unpressing the button) contact bounce
	}

}

void onclick() { // motor start/stop
	earlier_time_foot_pedal_pressed_time_ms = 0;
	
	if (stop_propogation) { // don't execute onclick event if onkeypress was executed
		stop_propogation = 0; 
	} else {
		if (led_blinking()) { // led blink off
				led_blink_speed = LED_BLINK_START_SPEED_PERCENT; // reset blink speed in earlier 'blinking session' to default start speed
				set_led_blink_speed(0);
			} else { // led blink on
				set_led_blink_speed(LED_BLINK_START_SPEED_PERCENT);
		}
	}
}

void onkeypress() { // led blink speed increase
	if (led_blinking() && foot_pedal_pressed_time_ms - earlier_time_foot_pedal_pressed_time_ms >= ONE_SECOND) {
		earlier_time_foot_pedal_pressed_time_ms = foot_pedal_pressed_time_ms;
		stop_propogation = 1;
		
		if (led_blink_speed < ONE_HUNDRED_PERCENT) {
			led_blink_speed += 30; // ms  speed increase
			
			set_led_blink_speed(led_blink_speed);
		}
	}
}