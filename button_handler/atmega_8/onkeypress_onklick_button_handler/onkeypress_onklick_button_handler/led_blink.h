#ifndef LED_BLINK_
#define LED_BLINK_

#include "app.h"

#define LED_BLINK_PORT D
#define LED_BLINK_PIN PD4
#define LED_BLINK_MIN_SPEED 1000 // ms
#define LED_BLINK_MAX_SPEED 200 // ms
#define LED_BLINK_SPEED_RANGE (LED_BLINK_MIN_SPEED - LED_BLINK_MAX_SPEED)
#define CONVERT_SPEED_PERCENT_TO_MS(percentage) LED_BLINK_MIN_SPEED - (percentage * LED_BLINK_SPEED_RANGE / 100)

void led_blink_init();
void led_on();
void led_off();

/*
	Call this from main loop function
*/
void led_blink_handler();

/*
	Return boolean
*/
int led_blinking();

/*
	0% - no blink and set led off
	100% - set led on and blink with max speed (LED_BLINK_MAX_SPEED)
	
	'unsigned long' use - because wen we calculating blink speed the speed can be bigger then int (65000)
*/
void set_led_blink_speed(unsigned long);

#endif /* LED_BLINK_ */