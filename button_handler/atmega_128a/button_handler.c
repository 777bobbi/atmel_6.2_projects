/*
 * Blink.c
 *
 * Created: 18.05.2013 19:15:01
 *  Author: Vladimir
 */ 

#define F_CPU 8000000UL

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
	DDRA = 0xFF; // data direction register - set all output (diodes) 
	DDRD = 0x00; // data direction register - set all input (buttons)
	
	PORTA = 0b11101110;
	PORTD = 0xFF; //pull up, set hi level
	
	while(1)
    {
		if ( bit_is_clear(PIND, 0) ) { //bit_is_clear(PIND, 0)    OR    !(PIND & (1 << PIND0))
			_delay_ms(30);
			if ( bit_is_clear(PIND, 0) ) {
				PORTA = ~PORTA; // invert port
				_delay_ms(1000);
				PORTA = ~PORTA; // invert port back
			}

		}			
    }
	
	return 0;
}