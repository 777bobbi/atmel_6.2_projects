/*
 * hall_sensor.c
 *
 * Created: 5/1/2014 8:51:39 PM
 *  Author: Vladimir
 */

#define F_CPU 16000000UL
#define HALL_SENSOR PD2

// Include Files
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "lib/lcd/lcd.h"	//	THE LCD HEADER FILE TO INCLUDE

void io_init(void);
void external_interrupts_init(void);
void show_count_results(void);

volatile unsigned long int revolution_counter = 0;

int main(void)
{
	io_init();
	external_interrupts_init();
	sei(); //Enable Global Interrupt
	lcd_init(LCD_DISP_ON);
		
	while(1)
	{
		show_count_results();
		_delay_ms(500);
	};
		
	return 0;
}

void io_init(void) 
{
	DDRD &= ~(1<<HALL_SENSOR); // data direction register. Set as input (interrupts buttons)
	PORTD |= 1<<HALL_SENSOR; //Pull up (set height level)
}

void external_interrupts_init(void) 
{
	//INT0_vect
	MCUCR |= 1<<ISC01|1<<ISC00;	// Trigger INT0 on rising edge.
	GICR |= 1<<INT0; // Enable interrupt INT0
	GIFR |= 1<<INT0; // Cleared flag by writing a logical one to it. This scaffolding need to avoid first interrupt occur, because interrupt pin set pull up.
}

void show_count_results()
{
	char destination[16];
	destination[15] = 0; //clear destination string, for output and nulled last byte
	
	sprintf(destination, "%ld\n", revolution_counter);
	
	lcd_clrscr();
	lcd_puts("hall sensor cnt.");
	lcd_gotoxy(0, 1);
	lcd_puts(destination);
}

//Interrupt Service Routine for INT0 rising edge
ISR(INT0_vect)
{
	revolution_counter += 1;
}
