/*
 * eeprom_test2.c
 *
 * Created: 9/2/2013 5:39:36 PM
 *  Author: Vladimir
 
 !!!!!!!!!!!! don't forget program both .hex && .eep !!!!!!!!!!!!!!
 
 */ 

#define F_CPU 8000000UL

#include <inttypes.h>
#include <avr/io.h>
#include <avr/iom128a.h>
#include <avr/eeprom.h>
#include <util/delay.h>

/*
** this global variables are stored in EEPROM
*/
uint32_t eeprom_var1 EEMEM = 42;

int main(void)
{
	uint32_t state1 = eeprom_read_dword( &eeprom_var1 );	//read variable from EEPROM
	//float     floatVar;

	DDRA   = 0xff;	// use all pins on port A for output
	PORTA  = 0xff;	// 0 - leds on, 1 - leds off

	_delay_ms(1000);
	
	if ( state1 == 42 ) {
		PORTA = ~42;
	}
	
	
	//eeprom_write_byte( &eeprom_var2, state1 );	//and write value to EEPROM addr 0003


	for(;;) {}    // loop forever
}