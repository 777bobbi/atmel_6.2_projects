/*
 * adc_plus_pwm.c
 *
 * Created: 09/02/2013 8:39:09 PM
 *  Author: Vladimir
 
 init io
 init adc
 init pwm
 set the initial pwm equal to zero
 analog reed
 analog write - set and keep the pwm on the according level
  
 connect LED to OC2/PB7 PIN !!!
 
 */ 

#define F_CPU 8000000UL
#define PIN_PWM DDRB7
#define PIN_ADC DDRF0
#define UART_BAUD_RATE 9600

#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom128a.h>
#include <util/delay.h>
#include <uart.h>
#include <math.h>

void init_io(void) {
	DDRB |= 1 << PIN_PWM; // set pin pwm like output.
	DDRF &= ~(1 << PIN_ADC); // set pin ADC like input.
}

void init_adc(void) {
	ADCSRA |= ((1<<ADPS2)|(1<<ADPS1));  // (ADCSRA - ADC Control and Status Register A) 8Mhz/64 = 125Khz the ADC reference clock
	ADMUX |= (1<<REFS0);                // (ADC Multiplexer Selection Register) Voltage reference from Avcc (5v) (Don't forgot to connect Vcc to AREF pin - the threshold level of voltage. )
	ADCSRA |= (1<<ADEN);                // Turn on ADC
	ADCSRA |= (1<<ADSC);                // Do an initial conversion because this one is the slowest and to ensure that everything is up and running
}

uint16_t read_adc(uint8_t channel) {
	ADMUX &= 0xF0;                   // (0b1111 0000) Clear the older channel that was read
	ADMUX |= channel;                // Defines the new ADC channel to be read
	ADCSRA |= (1<<ADSC);             // Starts a new conversion
	while(ADCSRA & (1<<ADSC));       // (0b0100 0000 & (0b0100 0000)) Wait until the conversion is done. (In Single Conversion mode, write bit ADSC to one to start each conversion.)
	return ADCW;                   	 // Returns the ADC value of the chosen channe
}

/*
 start timer:
 WGM20			- set 'PWM, Phase Correct' mode
 COM21, COM20	- set OC2 on compare match when up-counting. Clear OC2 on compare match when down-counting.
 CS20			- clkI/O (No prescaling)
 CS21, CS20		- clkI/64 = 125 KHz (From prescaler)
 CS22			- clkI/256 = 31250 Hz (From prescaler)
 CS22, CS20		- clkI/1024 = 7812 Hz (From prescaler)
*/
void init_pwm (void)
{
	TCCR2 = 1<<WGM20|1<<COM21|1<<COM20|1<<CS20;
	OCR2 = 0; // initial value on PWM pin output
}

void set_pwm(int val) {
	OCR2 = val;
}

int main(void)
{
    //char uart_output[] = "test\n";
	int pwm_value = 0;
	
	init_io();
	init_adc();
	init_pwm();
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	sei(); //enable interrupt, since UART library is interrupt controlled
	
	while(1) {
		//itoa((int)round(read_adc(PIN_ADC) / 4), uart_output, 10);
		//uart_puts(uart_output);
		//uart_putc('\n');
		//_delay_ms(50);
		pwm_value = (int)round(read_adc(PIN_ADC)/4);
		set_pwm(pwm_value);				
	};
	
	return 0;
}