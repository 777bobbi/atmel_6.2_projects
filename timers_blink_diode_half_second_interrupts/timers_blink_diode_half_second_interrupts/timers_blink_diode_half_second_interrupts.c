/*
 * timers_blink_diode_half_second_interrupts.c
 *
 * Created: 9/9/2013 5:19:25 PM
 *  Author: Vladimir
 
 We will now use our timer in CTC mode, in this mode the timer counts up to a pre-programmed 
 value, and then it generates an interrupt, resets itself and starts again counting from 0, 
 all this is done in hardware so this free�s up our micro to do more important tasks than setting 
 a timer back to 0 again.
 
 !!!!!!!!!!Important!!!!!!!!!!!
 
 There are some caveats about using interrupts that you should know right now, 
 one NEVER uses delays inside interrupts, interrupts are supposed to be fast and clean, 
 delaying inside an interrupt means that you are doing it in the wrong way, also, it is 
 good practice to not call functions inside interrupts, due to the overhead introduced 
 by the called functions, so remember, interrupts should be kept clean. If you want to 
 have a complex program that works say every 10ms, it is better set a flag in the 
 interrupt service routine and then use the flag in the main loop to know if the 
 interrupt as already fired or not, and last but not least EVERY variable that is 
 shared between interrupts and any other functions must be declared as volatile, 
 for example:

 int variable1 = 0;    //This is not a volatile variable and should no be used as a shared variable
 volatile int variable2 = 0;    //This is a volatile variable as you can see by the means of the volatile keyword
 
 
 To create an interrupt function there is also a special method to do them, they 
 are declared using a special keyword and they accept as argument the interrupt 
 vector/name, the vector names can be found in the section 11 of the manual, 
 page 58, called Interrupts, to the provided vector names in the table we need 
 to add the _vect keyword and replace all the spaces by _, let me show you how to do it, 
 also interrupt service routines don�t have any return so if you need information back 
 from an interrupt use a global variable declared as volatile, or use pointers:
 
 ISR(TIMER0_COMPA_vect){            //This is an interrupt service routine for the TIMER0 COMPA vector
	 //Put your code here
 }
 
 http://hekilledmywire.wordpress.com/2011/05/28/introduction-to-timers-tutorial-part-6/
 
 
 
 */ 


#define F_CPU 8000000UL
#define PIN_LED PORTA1

#include <avr/io.h>
#include <avr/iom128a.h>
#include <avr/interrupt.h>

int main(void){
	
	DDRA |= 1<<PIN_LED; //Set as output
	PORTA &= ~(1 << PIN_LED); // 1 - led off, 0 - led on
	
	//Configure timer in CTC mode
	TCCR1B = (1<<WGM12)|(1<<CS12);    //Enable CTC mode, and set prescaler to 1:256
	OCR1A =  15624;            //sets the desired count to generate the 2Hz signal   (Target Timer Count = (((Input Frequency / Prescaler) / Target Frequency) - 1) -> Target Timer Count = (((8000000Hz / 256) / 2Hz) - 1) = 15624)
	TIMSK = (1<<OCIE1A);        //Enable timer interrupts
	
	sei();    //Enable global interrupts, so our interrupt service routine can be called
	
	for(;;){
		//Our infinite loop, this will remain clean, everything is done in the interrupt
	}
	
	return 0;
}

ISR(TIMER1_COMPA_vect){        //This is our interrupt service routine
	
	PORTA ^= 1<<PIN_LED;    //Toggle the led state
}