/*
 * function_pointer_array.c
 *
 * Created: 9/10/2013 7:53:34 PM
 *  Author: Vladimir
 */ 

#define F_CPU 8000000UL
#define PIN_LED DDRA1

#include <avr/io.h>
#include <avr/iom128a.h>
#include <util/delay.h>

void (*pFunction[2])(void);

void someFunction1(void) {
	PORTA ^= 1<<PIN_LED;
}
void someFunction2(void) {
	PORTA ^= 1<<PIN_LED;
}

int main(void) {
	DDRA |= 1<<PIN_LED;
	PORTA &= ~(1<<PIN_LED); // 0 - led on, 1 - led off
	
	

	// Set function pointer
	pFunction[0] = &someFunction1;
	pFunction[1] = &someFunction2;
	
	while(1) {
		// Call function
		pFunction[1]();
		_delay_ms(500);
	}
}