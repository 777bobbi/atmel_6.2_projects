/* 
* System.h
*
* Created: 5/19/2014 8:09:59 AM
* Author: Vladimir
*/


#ifndef __SYSTEM_H__
#define __SYSTEM_H__


class System
{
//variables
public:
protected:
private:

//functions
public:
	System();
	~System();
protected:
private:
	System( const System &c );
	System& operator=( const System &c );

}; //System

#endif //__SYSTEM_H__
