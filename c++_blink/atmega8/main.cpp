/*
 * c___blink.cpp
 *
 * Created: 5/19/2014 8:05:13 AM
 *  Author: Vladimir
 */ 


#define F_CPU 16000000UL

#include <avr/io.h>
#include <inttypes.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define LED_RED PD4

int main(void)
{
	int number = 124;
	
	DDRD |= 1<<LED_RED;
	PORTD |= 1<<LED_RED;
	
	while(1)
	{
		number += 5;
		PORTD ^= 1<<LED_RED;
		_delay_ms(500);
	}
}