/*
 * keyboard_74HC165_buttons_handle_atmega16.c
 *
 * Created: 9/13/2013 1:53:54 PM
 *  Author: Vladimir
 
 How to call the function using function pointer - http://stackoverflow.com/questions/1952175/how-to-call-the-function-using-function-pointer
 
 //itoa (bytearray[i], uart_str, 2);
 //uart_puts(strcat(uart_str, " "));
 //sprintf(uart_str, "%d ", button_pressed_number);
 uart_puts(uart_str);
 char uart_str[20] = "\0"; // bite + " " + \0
 char uart_str[10];
 uart_str[0] = '\0';
 
 */ 

#define F_CPU 8000000UL
#define UART_BAUD_RATE 9600

#define L74HC165_DDR DDRD
#define L74HC165_PORT PORTD
#define L74HC165_PIN PIND
#define L74HC165_CLOCKPIN PORTD1 // CLK
#define L74HC165_LOADPIN PORTD0 // SH/LD (async. parallel load (PL) input (active L)
#define L74HC165_DATAPIN PORTD2 // SO (serial output from the last stage)
#define L74HC165_ICNUMBER 2 //setup number of chip attached to the board

#include <stdio.h>		// sprintf
#include <stdlib.h>		// itoa
#include <string.h>     // strcat
#include <avr/io.h>
#include <avr/iom128a.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <lib/uart/uart.h>

//Functions prototype
void io_init(void);		
void l74hc165_init();
void l74hc165_read(uint8_t *bytearray);
int l74hc165_read_byte();

int main(void)
{
	io_init();
	l74hc165_init();
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	sei(); //Enable global interrupts (UART, ...)
	
	for(;;) {
		
		l74hc165_read_byte();
		_delay_ms(100);
		
	}

}

void io_init(void) {
	//DDRA |= 1<<PA1; //Set as output
	//PORTA |= 1<<PA1;
	//PORTA	&= ~(1<<PIN_LED); // 1 - led off, 0 - led on, Proteus simulator - backwards
}

/*
 * init the shift register
 */
void l74hc165_init() {
	//output
	L74HC165_DDR |= (1 << L74HC165_CLOCKPIN);
	L74HC165_DDR |= (1 << L74HC165_LOADPIN);
	
	//input
	L74HC165_DDR &= ~(1 << L74HC165_DATAPIN);
	
	//low
	L74HC165_PORT &= ~(1 << L74HC165_LOADPIN);
	L74HC165_PORT &= ~(1 << L74HC165_CLOCKPIN);
}


/*
	freeze state
*/
void l74hc165_load(){
	L74HC165_PORT &= ~(1 << L74HC165_LOADPIN);
	L74HC165_PORT |= (1 << L74HC165_LOADPIN);
}

/*
	shift byte
*/
void l74hc165_clock() {
	L74HC165_PORT &= ~(1 << L74HC165_CLOCKPIN);
	L74HC165_PORT |= (1 << L74HC165_CLOCKPIN);
}

int l74hc165_read_byte() {
	l74hc165_load();

	int data = 0;
	char buf[20];
	
	for (int i=0; i<8; i++) {
		//data |= (bit_is_set(PINA,7)?1:0)<<(7-i);
		
		if ( bit_is_set(L74HC165_PIN, L74HC165_DATAPIN) ) {
			sprintf(buf, "1");
			uart_puts(buf);
			} else {
			sprintf(buf, "0");
			uart_puts(buf);
		}
		
		
		
		//data |= ( (L74HC165_PIN & (1 << L74HC165_DATAPIN))>>L74HC165_DATAPIN ) << (7-i);
		//data |= digitalRead(_DATA_Pin)<<(7-i);
		l74hc165_clock();
	}
	
	sprintf(buf, "   \n");
	uart_puts(buf);	
	//_delay_us(100);
	return data;
}