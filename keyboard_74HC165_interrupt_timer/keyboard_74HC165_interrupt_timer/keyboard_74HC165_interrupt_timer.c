/*
 * keyboard_74HC165_interrupt_timer.c
 *
 * Created: 9/9/2013 1:48:18 PM
 *  Author: Vladimir
 
 How to call the function using function pointer - http://stackoverflow.com/questions/1952175/how-to-call-the-function-using-function-pointer
 
 
 */ 

#define F_CPU 8000000UL
#define UART_BAUD_RATE 9600

#include <stdio.h>		// sprintf
#include <stdlib.h>		// itoa
#include <string.h>     // strcat
#include <avr/io.h>
#include <avr/iom128a.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <uart.h>
#include <l74hc165.h>

uint8_t get_icarray[L74HC165_ICNUMBER];
volatile unsigned long milis = 0;    //This is our shared volatile variable that will be used as the millisecond count

void timer1_init(void);
void poll_buttons();

int main(void)
{	
	timer1_init();
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	l74hc165_init();
	sei(); //Enable global interrupts (UART, ...)
	
	for(;;) {
		
		if ( milis >= 500 ) {
			milis = 0;
			poll_buttons();
		}

			
	}

}

void timer1_init(void){
	TCCR1B	|= 1<<WGM12;			//Timer in CTC mode
	TCCR1B	|= 1<<CS11|1<<CS10;		//Prescaler to 1:64/F_CPU:64
	OCR1A	= 124;					//Value to have an compare at every 1ms = 1000Hz (Target Timer Count = (((Input Frequency / Prescaler) / Target Frequency) - 1) -> Target Timer Count = (((8000000Hz / 64) / 1000Hz) - 1) = 124)
	TIMSK	= 1<<OCIE1A;			//Enable timer interrupts
}

void poll_buttons() {
	char uart_str[20];// = "\0"; // bite + " " + \0
	uint8_t bytearray[L74HC165_ICNUMBER];
	
	l74hc165_shiftin(bytearray); //read data

	for(uint8_t i = 0; i < L74HC165_ICNUMBER; i++) {
		itoa (bytearray[i], uart_str, 2);
		uart_puts(strcat(uart_str, " "));
	}
	
	uart_puts("  ");
}

ISR(TIMER1_COMPA_vect){ //This is our interrupt service routine
	milis += 1; //Increase milis count by one millisecond
}