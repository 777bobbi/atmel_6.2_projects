/*
 * pwm_blinking_diod.c
 *
 * Created: 8/29/2013 4:08:40 PM
 *  Author: Vladimir
 */ 

#define F_CPU 8000000UL

#include <avr/io.h>
#include <avr/iom128a.h>
#include <util/delay.h>

void init_io(void) {
	DDRB |= 1 << DDRB7; // data direction register, PORT_PWM; DDRB7 - PIN_PWM - set like output 
}

/*
 start timer:
 WGM20			- set 'PWM, Phase Correct' mode
 COM21, COM20	- set OC2 on compare match when up-counting. Clear OC2 on compare match when down-counting.
 CS20			- clkI/O (No prescaling)
*/
void init_pwm (void)
{
	TCCR2 = 1<<WGM20|1<<COM21|1<<COM20|1<<CS20;
	OCR2=0x00; // initial value on PWM pin output
}

int main(void)
{
	init_io();
	init_pwm();
	
	while ( 1 ) {
		for (int i = 0; i < 255; i++) {
			OCR2++;
			_delay_ms(10);
		}
		for (int i = 0; i < 255; i++) {
			OCR2--;
			_delay_ms(10);
		}
	}
	
	return 0;
}