/*
 * app.cpp
 *
 * Created: 3/1/2015 2:28:31 PM
 *  Author: Vladimir
 */ 

#include "app.h"

void setup()
{
	io_init();
	lcd_init(LCD_DISP_ON);
	
	lcd_clrscr();
	lcd_puts("Testing.. 1.2.3...");
	lcd_gotoxy(0, 1);
	lcd_puts("Hi! Programmer!");
}

void loop()
{
	TOGL(D, LED_RED); // the same as PORTD ^= 1<<LED_RED;
	_delay_ms(1000);
}