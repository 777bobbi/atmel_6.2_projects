/*
 * app.h
 *
 * Created: 3/1/2015 2:27:39 PM
 *  Author: Vladimir
 */ 


#ifndef APP_H_
#define APP_H_

#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>

// C libraries must be include hear
extern "C" {
	#include "lcd.h"
}

// For Bitvise Operation Simplification Defines
#define CLR(port,pin)	PORT ## port &= ~(1<<pin)
#define SET(port,pin)	PORT ## port |=  (1<<pin)
#define TOGL(port,pin)	PORT ## port ^=  (1<<pin)
#define READ(port,pin)	PIN  ## port &   (1<<pin)
#define OUT(port,pin)	DDR  ## port |=  (1<<pin)
#define IN(port,pin)	DDR  ## port &= ~(1<<pin)

#define LED_RED PD4

// initialization function prototypes
void io_init();

// the main app function prototypes
void setup();
void loop();


#endif /* APP_H_ */