/*
 * init.cpp
 *
 * Created: 3/1/2015 2:34:55 PM
 *  Author: Vladimir
 */ 

#include "app.h"

void io_init() {
	// LCD
	OUT(C,4); OUT(C,5); OUT(C,6);	// RS - RW - E
	
	// Red led
	OUT(D, 4); //the same as DDRD |= 1<<LED_RED; PORTD |= 1<<LED_RED;
}