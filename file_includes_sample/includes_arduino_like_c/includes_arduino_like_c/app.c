/*
 * app.c
 *
 * The 'code sugar' entry point into application
 *
 * Created: 3/1/2015 2:08:15 PM
 *  Author: Vladimir
 */ 

#include "app.h"

void setup()
{
	io_init();
}

void loop()
{
	PORTD ^= 1<<LED_RED;
	_delay_ms(500);
}