/*
 * app.h
 *
 * This file should contain only constant, macros definitions and function prototypes
 *
 * Created: 3/1/2015 1:06:08 PM
 *  Author: Vladimir
 */ 

#ifndef APP_H_
#define APP_H_

#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>

#define LED_RED PD4

// the main app function prototypes
void setup();
void loop();

// initialization function prototypes
void io_init();

#endif /* APP_H_ */