/*
 * init.c
 *
 * This file contain definitions of initialization functions of our app for example: io_init, interrapts_init, timer/counter init and so on...
 * Also it contain ISR() functions (general interrupt service routine for all interrupts)
 *
 * Created: 3/1/2015 1:05:57 PM
 *  Author: Vladimir
 */ 

#include "app.h"

void io_init() {
	DDRD |= 1<<LED_RED;
	PORTD |= 1<<LED_RED;
}