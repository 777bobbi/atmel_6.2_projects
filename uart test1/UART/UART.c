/*
 * UART.c
 *
 * Created: 8/28/2013 10:08:59 AM
 *  Author: Vladimir
 
  // USART initialization
  // Communication Parameters: 8 Data, 1 Stop, No Parity
  // USART Receiver: On
  // USART Transmitter: Off
  // USART0 Mode: Asynchronous
  // USART Baud Rate: 19200
  UCSR0A=0x00;
  UCSR0B=0x90;
  UCSR0C=0x06;
  UBRR0H=0x00;
  UBRR0L=0x2F;
 
 
 */
 
#define F_CPU 8000000UL

#include <avr/io.h>
#include <avr/iom128a.h>

#define FOSC 8000000// Clock Speed
#define BAUD 9600
#define MYUBRR FOSC/16/BAUD-1

void USART_Init( unsigned int ubrr );
void USART_Transmit( unsigned char data );

int main( void )
{
	USART_Init ( MYUBRR );
	
	while(1) {
		USART_Transmit('a');
	}
	
	return 0;	
}

void USART_Init( unsigned int ubrr )
{
	/* Set baud rate */
	UBRR0H = (unsigned char)(ubrr>>8); // UBRRL & UBRRH � USART Baud Rate Registers
	UBRR0L = (unsigned char)ubrr;
	/* Enable receiver and transmitter */
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	/* Set frame format: 8data, 2stop bit */
	UCSR0C = (1<<USBS0)|(3<<UCSZ00);
}

void USART_Transmit( unsigned char data )
{
	/* Wait for empty transmit buffer */
	while ( !( UCSR0A & (1<<UDRE0)) );
	/* Put data into buffer, sends the data */
	UDR0 = data;
}