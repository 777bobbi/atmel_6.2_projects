/*
 * lcd_test.c
 *
 * Created: 5/1/2014 6:59:19 PM
 *  Author: Vladimir
 */ 


#define F_CPU 8000000UL

// For Bitvise Operation Simplification Defines
#define CLR(port,pin)	PORT ## port &= ~(1<<pin)
#define SET(port,pin)	PORT ## port |=  (1<<pin)
#define TOGL(port,pin)	PORT ## port ^=  (1<<pin)
#define READ(port,pin)	PIN  ## port &   (1<<pin)
#define OUT(port,pin)	DDR  ## port |=  (1<<pin)
#define IN(port,pin)	DDR  ## port &= ~(1<<pin)

// Include Files
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "lib/lcd/lcd.h"	//	THE LCD HEADER FILE TO INCLUDE

int main(void)
{
	// LCD
	OUT(C,4); OUT(C,5); OUT(C,6);	// RS - RW - E
	lcd_init(LCD_DISP_ON);
	lcd_clrscr();
	lcd_puts("Testing.. 1.2.3...");
	lcd_gotoxy(0, 1);
	lcd_puts("Hello! Proger!");
	
	while(1);
}