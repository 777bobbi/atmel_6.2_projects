/*
 * function_generation_atmega8.c
 *
 * Created: 9/24/2013 1:08:44 AM
 *  Author: Vladimir
 */ 


#include <avr/io.h>
#include <avr/iom8.h>


#define F_CPU 8000000UL
#define PIN_PWM DDRB7


void init_io(void) {
	DDRB |= 1 << PIN_PWM; // set pin pwm like output.
}

/*
 start timer:
 WGM20			- set 'PWM, Phase Correct' mode
 COM1A1			- Clear OC1A/OC1B on Compare Match, set OC1A/OC1B at BOTTOM, (non-inverting mode)
 CS10			- clkI/O (No prescaling)
 
 CS21, CS20		- clkI/64 = 125 KHz (From prescaler)
 CS22			- clkI/256 = 31250 Hz (From prescaler)
 CS22, CS20		- clkI/1024 = 7812 Hz (From prescaler)
*/
void init_pwm (void)
{
	TCCR1A = 1<<WGM20|1<<COM21|1<<COM20|1<<CS20;
	OCR2 = 0; // initial value on PWM pin output
}

void set_pwm(int val) {
	OCR2 = val;
}

int main(void)
{
	
	init_io();
	init_adc();
	init_pwm();
	
    while(1)
    {
        set_pwm(50); 
    }
}