/*
 * keyboard_74HC165.c
 *
 * Created: 9/9/2013 7:29:04 AM
 *  Author: Vladimir
 
 !!!!!!!!!!!!!!! Don't forgot configure l74hc165.h !!!!!!!!!!!!!!!
 
 */ 

#define F_CPU 8000000UL
#define UART_BAUD_RATE 9600

#include <stdio.h>		// sprintf 
#include <stdlib.h>		// itoa 
#include <string.h>     // strcat 
#include <avr/io.h>
#include <avr/iom128a.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <uart.h>
#include <l74hc165.h>

uint8_t get_icarray[L74HC165_ICNUMBER];

const char *byte_to_binary(int x)
{
	static char b[9];
	b[0] = '\0';

	int z;
	for (z = 128; z > 0; z >>= 1)
	{
		strcat(b, ((x & z) == z) ? "1" : "0");
	}

	return b;
}

int main(void)
{
	char str[10] = "\0"; // bite + " " + \0
	uint8_t bytearray[L74HC165_ICNUMBER];
	
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	l74hc165_init();

	sei(); //enable interrupt, since UART library is interrupt controlled

	for(;;) {
		//read data
		l74hc165_shiftin(bytearray);

		//print data
		for(uint8_t i=0; i<L74HC165_ICNUMBER; i++) {
			//sprintf(str, "%s ", byte_to_binary(bytearray[i]));
			itoa (bytearray[i], str, 2);
			strcat(str, " ");
			uart_puts(str);
		}
		
		uart_puts("  ");
		_delay_ms(1000);
	}

}