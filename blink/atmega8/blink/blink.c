/*
 * blink.c
 *
 * Created: 5/1/2014 6:52:00 PM
 *  Author: Vladimir
 */

#define F_CPU 8000000UL

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
    DDRD = 0xFF; // data direction - output
	
	PORTD = 0xFF; // all pull up (diodes on)
	
	while(1)
    {
		PORTD ^= 0xFF; // (0b1111 1111) PORTD ^= (1 << PD5); - blink only PD5
		_delay_ms(1000);	
    }
	
	return 0;
}