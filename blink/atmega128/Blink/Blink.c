/*
 * Blink.c
 *
 * Created: 18.05.2013 19:15:01
 *  Author: Vladimir
 */ 

#define F_CPU 8000000UL

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
    DDRA = 0xFF; // data direction - output
	
	PORTA = 0x00; // all pull down (diodes on)
	
	while(1)
    {
		PORTA ^= 0xFF; // (0b1111 1111) PORTA ^= (1 << PB5); - blink only PB5
		_delay_ms(1000);	
    }
	
	return 0;
}