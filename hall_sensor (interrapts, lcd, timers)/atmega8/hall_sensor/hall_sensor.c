/*
 * hall_sensor.c
 *
 * Created: 5/1/2014 8:51:39 PM
 *  Author: Vladimir
 */

#define F_CPU 16000000UL
#define HALL_SENSOR PD2

// For Bitwise Operation Simplification Defines
#define CLR(port,pin)	PORT ## port &= ~(1<<pin)
#define SET(port,pin)	PORT ## port |=  (1<<pin)
#define TOGL(port,pin)	PORT ## port ^=  (1<<pin)
#define READ(port,pin)	PIN  ## port &   (1<<pin)
#define OUT(port,pin)	DDR  ## port |=  (1<<pin)
#define IN(port,pin)	DDR  ## port &= ~(1<<pin)

// Include Files
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "lib/lcd/lcd.h"	//	THE LCD HEADER FILE TO INCLUDE

void io_init(void);
void external_interrupts_init(void);
void timercounter2_8bit_init(void);
void show_count_results(void);

volatile unsigned long int revolution_counter = 0;
volatile unsigned long int usecond_dozens = 0;
volatile unsigned int start_count = 0;

int main(void)
{
	io_init();
	external_interrupts_init();
	timercounter2_8bit_init();
	sei(); //Enable Global Interrupt
	lcd_init(LCD_DISP_ON);
		
	while(1)
	{
		show_count_results();
		_delay_ms(1000);
	};
		
	return 0;
}

void io_init(void) 
{
	DDRD &= ~(1<<HALL_SENSOR); // data direction register. Set as input (interrupts buttons)
	PORTD |= 1<<HALL_SENSOR; //Pull up (set height level)
}

void external_interrupts_init(void) 
{
	//INT0_vect
	MCUCR |= 1<<ISC01|1<<ISC00;	// Trigger INT0 on rising edge.
	GICR |= 1<<INT0; // Enable interrupt INT0
	GIFR |= 1<<INT0; // Cleared flag by writing a logical one to it. This scaffolding need to avoid first interrupt occur, because interrupt pin set pull up.
}

//Execute interrupt every ten useconds
void timercounter2_8bit_init(void)
{
	TCCR2 |= 1<<WGM21; //Configure timer in CTC mode
	TCCR2 |= 1<<CS21|1<<CS20; //Set prescaler to 1:32 (F_CPU:32) and this action enable timer
	OCR2 = 4; //Value to have an compare at every 10us = 100000Hz (Target Timer Count = (((Input Frequency / Prescaler) / Target Frequency) - 1) -> Target Timer Count = (((16000000Hz / 32) / 100000Hz) - 1) = 4)
	TIMSK |= 1<<OCIE2; //Enable timer interrupts
}

void show_count_results()
{
	char destination[16];
	destination[15] = 0; //clear destination string, for output and nulled last byte
	
	sprintf(destination, "%ld\n", revolution_counter);
	
	lcd_clrscr();
	lcd_puts("hall sensor cnt.");
	lcd_gotoxy(0, 1);
	lcd_puts(destination);
}

//Interrupt Service Routine for INT0
ISR(INT0_vect)
{
	start_count = 1;
	
	if (usecond_dozens >= 1)
	{
		revolution_counter += 1;
		usecond_dozens = 0;
		start_count = 0;
	}
}

//This interrupt service routine executes every ten microseconds
ISR(TIMER2_COMP_vect)
{
	if (start_count)
	{
		usecond_dozens++;
	}
}