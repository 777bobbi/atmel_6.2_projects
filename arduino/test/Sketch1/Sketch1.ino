/*
 * Sketch1.ino
 *
 * Created: 5/19/2014 8:30:46 AM
 * Author: Vladimir
 */ 

#include "MyClass.h"

void setup()
{
	myClass.setup();
}

void loop()
{
	myClass.loop();
}
