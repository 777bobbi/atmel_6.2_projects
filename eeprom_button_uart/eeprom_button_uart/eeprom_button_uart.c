/*
 * eeprom_button_uart.c
 *
 * Created: 9/2/2013 6:40:30 PM
 *  Author: Vladimir
 */ 

#define F_CPU 8000000UL
#define UART_BAUD_RATE 9600
#define PIN_ADC DDRF0
#define PIN_BUTTON0 PIND0
#define PIN_BUTTON1 PIND1

#include <stdio.h> // for sprintf
#include <stdlib.h>
#include <avr/interrupt.h>
#include <inttypes.h>
#include <avr/io.h>
#include <avr/iom128a.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <uart.h>

/*
 this global variables are stored in EEPROM
*/
uint32_t eeprom_var EEMEM = 42;

void init_io() {
	DDRF &= ~(1 << PIN_ADC); // set pin ADC like input.
	DDRA = 0xFF; // Configure all as output (diodes)
	DDRD |= 1<<PIN_BUTTON0|1<<PIN_BUTTON1; // Configure as input (buttons)
	
	PORTA  = 0xff;	// 0 - leds on, 1 - leds off
	PORTD |= 1<<PIN_BUTTON0|1<<PIN_BUTTON1; //pull up, set hi level	- buttons closing on ground
}

void uart_print(int val) {
	char str[10];
	
	sprintf(str,"%d\n", val);
	uart_puts(str);
	//_delay_ms(50);	
}

void blink_led() {
	PORTA = ~PORTA; // invert port
	_delay_ms(50);
	PORTA = ~PORTA; // invert port back	
}

int main(void)
{
	uint32_t state1 = eeprom_read_dword( &eeprom_var );	//read variable from EEPROM
	
	init_io();
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	sei(); //enable interrupt, since UART library is interrupt controlled
	
	while(1)
    {
        if ( bit_is_clear(PIND, PIN_BUTTON0) ) { // bit_is_clear(PIND, 0) OR !(PIND & (1 << PIND0))
	        _delay_ms(30);
	        if ( bit_is_clear(PIND, PIN_BUTTON0) ) {
		        blink_led();
				eeprom_write_dword(&eeprom_var, ++state1);
		        state1 = eeprom_read_dword( &eeprom_var );
		        uart_print(state1);
	        }
        }
		
		if ( bit_is_clear(PIND, PIN_BUTTON1) ) {
			_delay_ms(30);
			if ( bit_is_clear(PIND, PIN_BUTTON1) ) {
				blink_led();
				eeprom_write_dword(&eeprom_var, --state1);
				state1 = eeprom_read_dword( &eeprom_var );
				uart_print(state1);
			}
		}
		
		
    }
}