/*
 * timers_blink_diode_half_second.c
 *
 * Created: 9/9/2013 2:30:43 PM
 *  Author: Vladimir
 
 http://hekilledmywire.wordpress.com/2011/05/28/introduction-to-timers-tutorial-part-6/
 
 for dev board "atmega 128a":
 buttons: PORTD (PORTD0:3)
 leds: PORTA
 
 We will toggle the led at a rate of 2Hz, this means that the led will be on for 500ms and off for another 500ms.
 So we need to configure is to put our timer in normal mode, then select the prescaler values and then turn the timer on.
 
 */

#define F_CPU 8000000UL
#define PIN_LED PORTA0

#include <avr/io.h>
#include <avr/iom128a.h>

int main(void){
	
	DDRA |= 1<<PIN_LED; //Set as output
	PORTA &= ~(1 << PIN_LED); // 1 - led off, 0 - led on
	
	//Configure timer
	TCCR1B = 1<<CS12;    //clkI/O : 256 = 31250 Hz - prescaler
	
	for(;;){
		//Read timer value and act according with it
		if(TCNT1 >= 15624){      //Our pre-calculated timer count. (Target Timer Count = (((Input Frequency / Prescaler) / Target Frequency) - 1) -> Target Timer Count = (((8000000Hz / 256) / 2Hz) - 1) = 15624)
			PORTA ^= 1<<PIN_LED;    //Toggle the led state
			TCNT1 = 0;            //Reset the timer value
		}
	}
	
	return 0;
}