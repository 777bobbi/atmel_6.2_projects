/*
 * 
 *
 * Created: 9/12/2013 7:23:22 PM
 *  Author: Vladimir
 */ 

#define F_CPU 8000000UL

#include <avr/io.h>
#include <avr/iom16.h>

// For Bitvise Operation Simplification Defines
#define CLR(port,pin)	PORT ## port &= ~(1<<pin)
#define SET(port,pin)	PORT ## port |=  (1<<pin)
#define TOGL(port,pin)	PORT ## port ^=  (1<<pin)
#define READ(port,pin)	PIN  ## port &   (1<<pin)
#define OUT(port,pin)	DDR  ## port |=  (1<<pin)
#define IN(port,pin)	DDR  ## port &= ~(1<<pin)

// Function prototypes common
void io_init(void);

int main(void) {
	io_init();
	
	for (;;);
	
	return 0;
}

void io_init() {
	// LEDs
	DDRA = 255;
	DDRB = 255;
	DDRC = 255;
	DDRD = 255;
	
	PORTA = 255;
	PORTB = 255;
	PORTC = 255;
	PORTD = 255;
}