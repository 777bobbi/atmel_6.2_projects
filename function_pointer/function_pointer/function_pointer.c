/*
 * function_pointer.c
 *
 * Created: 9/10/2013 7:40:24 PM
 *  Author: Vladimir
 */ 

#define F_CPU 8000000UL
#define PIN_LED DDRA0

#include <avr/io.h>
#include <avr/iom128a.h>
#include <util/delay.h>

void someFunction(void) {
	PORTA ^= 1<<PIN_LED;
}

int main(void) {
	DDRA |= 1<<PIN_LED;
	PORTA &= ~(1<<PIN_LED); // 0 - led on, 1 - led off
	
	void (*pFunction)(void);

	// Set function pointer
	pFunction = &someFunction;

	while(1) {
		// Call function
		pFunction();
		_delay_ms(500);
	}
}