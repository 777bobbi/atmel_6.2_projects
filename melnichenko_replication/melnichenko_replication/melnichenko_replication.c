/*
 * melnichenko_replication.c
 *
 * Created: 10/5/2013 2:11:37 AM
 *  Author: Vladimir
 */ 

#include <avr/io.h>
#include <math.h>

#define F_CPU 16000000UL
#define PWM_PIN PB3 // OC2
#define PWM_DUTY_CYCLE_PERCENT 90
#define COUNTER2_MAX 256

void io_init(void);
void pwm_init(void);

int main(void)
{
    io_init();
	pwm_init();
	
	while(1)
    {
        //TODO:: Please write your application code 
    }
}

void io_init(void) {
	DDRB |= 1<<PWM_PIN;
	PORTB |= 1<<PWM_PIN;
}

void pwm_init() {
	TCCR2 |= 1<<WGM21 | 1<<WGM20; // Fast PWM
	TCCR2 |= 1<<COM21; // Clear OC2 on Compare Match, set OC2 at BOTTOM, (non-inverting mode)
	TCCR2 |= 1<<CS20 | 1<<CS21; // Prescaling - 32
	OCR2 = round(PWM_DUTY_CYCLE_PERCENT*COUNTER2_MAX/100);
}